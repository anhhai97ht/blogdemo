<!DOCTYPE html>
<html <?php language_attributes() ?>>

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>
    <?php if (is_home()): ?>
      <?php bloginfo('name') ?>
      <?php elseif( is_single() ): ?>
        <?php echo get_field('seo_title'); ?>
        <?php else: ?>
          <?php wp_title('', true,''); ?>
        <?php endif ?>
      </title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">

      <?php if (is_home()): ?> <!-- Khi ở trang chủ -->
      <meta name="description" content="<?php bloginfo('description') ?>" />
      <?php elseif(is_single()): ?> <!-- Khi ở trang bài viết -->
      <meta name="description" content="<?php echo get_field('seo_description'); ?>" />
      <meta name="keywords" content="<?php echo get_field('seo_keywords'); ?>" />
    <?php endif ?>
    <link href="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/css/blog-home.css" rel="stylesheet">
    <link href="<?php echo get_template_directory_uri() ?>/style.css" rel="stylesheet">

    <script defer="" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/jquery-1.10.2.min.js.tải xuống"></script>
    <script defer="" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/js.template.min.js.tải xuống"></script>
    <script defer="" type="text/javascript" src="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/js/script.min.js.tải xuống"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/css/index.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri() ?>/vendor/bootstrap/css/style.min.css">



    <link rel="shortcut icon" type="image/png" href="<?php echo get_template_directory_uri() ?>/images/favicon.ico"/>
  </head>

  <body style="padding-top: 0px; background: #f8f8f8;">

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-white bg-white <?php echo is_home() ? 'mb-0' : '' ?>">
      <div class="container">
        <div class="row">
          <div class="col-lg-2 col-12" id="logo">
            <a  href="<?php echo home_url() ?>" class="navbar-brand"  ><img src="<?php echo get_template_directory_uri() ?>/images/logo.png"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
          </div>
          <div class="col-lg-1 col-12">
            <span class="showCate navbar-brand"  href="#">
              <span class="icon"></span>
              <div class="listCategory">
                <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 200px; height: 345px; ">
                  <ul class="listParentCate" style="overflow: hidden; width: 200px; height: 345px; overflow: scroll;">
                    <li>
                      <a cid="169" href="https://123doc.org/doc-cat/169-luan-van-bao-cao.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click" class="show">Luận Văn - Báo Cáo</a>
                    </li>
                    <li>
                      <a cid="154" href="https://123doc.org/doc-cat/154-ky-nang-mem.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Kỹ Năng Mềm</a>
                    </li>
                    <li>
                      <a cid="672" href="https://123doc.org/doc-cat/672-mau-slide.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Mẫu Slide</a>
                    </li>
                    <li>
                      <a cid="35" href="https://123doc.org/doc-cat/35-kinh-doanh-tiep-thi.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Kinh Doanh - Tiếp Thị</a>
                    </li>
                    <li>
                      <a cid="44" href="https://123doc.org/doc-cat/44-kinh-te-quan-ly.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Kinh Tế - Quản Lý</a>
                    </li>
                    <li>
                      <a cid="54" href="https://123doc.org/doc-cat/54-tai-chinh-ngan-hang.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Tài Chính - Ngân Hàng</a>
                    </li>
                    <li>
                      <a cid="49" href="https://123doc.org/doc-cat/49-bieu-mau-van-ban.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Biểu Mẫu - Văn Bản</a>
                    </li>
                    <li>
                      <a cid="99" href="https://123doc.org/doc-cat/99-giao-duc-dao-tao.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Giáo Dục - Đào Tạo</a>
                    </li>
                    <li>
                      <a cid="284" href="https://123doc.org/doc-cat/284-giao-an-bai-giang.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Giáo án - Bài giảng</a>
                    </li>
                    <li>
                      <a cid="62" href="https://123doc.org/doc-cat/62-cong-nghe-thong-tin.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Công Nghệ Thông Tin</a>
                    </li>
                    <li>
                      <a cid="85" href="https://123doc.org/doc-cat/85-ky-thuat-cong-nghe.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Kỹ Thuật - Công Nghệ</a>
                    </li>
                    <li>
                      <a cid="73" href="https://123doc.org/doc-cat/73-ngoai-ngu.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Ngoại Ngữ</a>
                    </li>
                    <li>
                      <a cid="93" href="https://123doc.org/doc-cat/93-khoa-hoc-tu-nhien.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Khoa Học Tự Nhiên</a>
                    </li>
                    <li>
                      <a cid="121" href="https://123doc.org/doc-cat/121-y-te-suc-khoe.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Y Tế - Sức Khỏe</a>
                    </li>
                    <li>
                      <a cid="104" href="https://123doc.org/doc-cat/104-van-hoa-nghe-thuat.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Văn Hóa - Nghệ Thuật</a>
                    </li>
                    <li>
                      <a cid="165" href="https://123doc.org/doc-cat/165-nong-lam-ngu.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Nông - Lâm - Ngư</a>
                    </li>
                    <li>
                      <a cid="186" href="https://123doc.org/doc-cat/186-the-loai-khac.htm" gtm-element="GTM_Header_Hover_Danhmuc_Click" gtm-label="GTM_Header_Hover_Danhmuc_Click">Thể loại khác</a>
                    </li>
                  </ul>
                  <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 205.926px;"></div>
                  <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                </div>
                <div class="listSub" style="background-color: rgb(10, 168, 136); display: block;">
                  <h2>Luận Văn - Báo Cáo</h2>
                  <div class="sub_nav_1" data-rel="1">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 200px; height: 280px;">
                      <ul style="overflow: scroll; width: 200px; height: 280px;">
                        <li class="nav_cat_54 hidden"><a cid="1846" href="https://123doc.org/doc-cat/1846-bao-cao-tai-chinh.htm" onmousemove="showNavCat(this, 2)">Báo cáo tài chính</a></li>
                        <li class="nav_cat_54 hidden"><a cid="55" href="https://123doc.org/doc-cat/55-ngan-hang-tin-dung.htm" onmousemove="showNavCat(this, 2)">Ngân hàng - Tín dụng</a></li>
                        <li class="nav_cat_54 hidden"><a cid="56" href="https://123doc.org/doc-cat/56-ke-toan-kiem-toan.htm" onmousemove="showNavCat(this, 2)">Kế toán - Kiểm toán</a></li>
                        <li class="nav_cat_54 hidden"><a cid="57" href="https://123doc.org/doc-cat/57-tai-chinh-doanh-nghiep.htm" onmousemove="showNavCat(this, 2)">Tài chính doanh nghiệp</a></li>
                        <li class="nav_cat_54 hidden"><a cid="58" href="https://123doc.org/doc-cat/58-dau-tu-chung-khoan.htm" onmousemove="showNavCat(this, 2)">Đầu tư Chứng khoán</a></li>
                        <li class="nav_cat_54 hidden"><a cid="59" href="https://123doc.org/doc-cat/59-dau-tu-bat-dong-san.htm" onmousemove="showNavCat(this, 2)">Đầu tư Bất động sản</a></li>
                        <li class="nav_cat_54 hidden"><a cid="60" href="https://123doc.org/doc-cat/60-bao-hiem.htm" onmousemove="showNavCat(this, 2)">Bảo hiểm</a></li>
                        <li class="nav_cat_54 hidden"><a cid="61" href="https://123doc.org/doc-cat/61-quy-dau-tu.htm" onmousemove="showNavCat(this, 2)">Quỹ đầu tư</a></li>
                        <li class="nav_cat_73 hidden"><a cid="1847" href="https://123doc.org/doc-cat/1847-luan-van-bao-cao-ngoai-ngu.htm" onmousemove="showNavCat(this, 2)">Luận văn báo cáo - ngoại ngữ</a></li>
                        <li class="nav_cat_73 hidden"><a cid="80" href="https://123doc.org/doc-cat/80-toefl-ielts-toeic.htm" onmousemove="showNavCat(this, 2)">TOEFL - IELTS - TOEIC</a></li>
                        <li class="nav_cat_73 hidden"><a cid="78" href="https://123doc.org/doc-cat/78-ngu-phap-tieng-anh.htm" onmousemove="showNavCat(this, 2)">Ngữ pháp tiếng Anh</a></li>
                        <li class="nav_cat_73 hidden"><a cid="77" href="https://123doc.org/doc-cat/77-anh-ngu-pho-thong.htm" onmousemove="showNavCat(this, 2)">Anh ngữ phổ thông</a></li>
                        <li class="nav_cat_73 hidden"><a cid="75" href="https://123doc.org/doc-cat/75-anh-van-thuong-mai.htm" onmousemove="showNavCat(this, 2)">Anh văn thương mại</a></li>
                        <li class="nav_cat_73 hidden"><a cid="76" href="https://123doc.org/doc-cat/76-anh-ngu-cho-tre-em.htm" onmousemove="showNavCat(this, 2)">Anh ngữ cho trẻ em</a></li>
                        <li class="nav_cat_73 hidden"><a cid="81" href="https://123doc.org/doc-cat/81-ky-nang-nghe-tieng-anh.htm" onmousemove="showNavCat(this, 2)">Kỹ năng nghe tiếng Anh</a></li>
                        <li class="nav_cat_73 hidden"><a cid="74" href="https://123doc.org/doc-cat/74-ky-nang-noi-tieng-anh.htm" onmousemove="showNavCat(this, 2)">Kỹ năng nói tiếng Anh</a></li>
                        <li class="nav_cat_73 hidden"><a cid="82" href="https://123doc.org/doc-cat/82-ky-nang-doc-tieng-anh.htm" onmousemove="showNavCat(this, 2)">Kỹ năng đọc tiếng Anh</a></li>
                        <li class="nav_cat_73 hidden"><a cid="83" href="https://123doc.org/doc-cat/83-ky-nang-viet-tieng-anh.htm" onmousemove="showNavCat(this, 2)">Kỹ năng viết tiếng Anh</a></li>
                        <li class="nav_cat_73 hidden"><a cid="79" href="https://123doc.org/doc-cat/79-chung-chi-a-b-c.htm" onmousemove="showNavCat(this, 2)">Chứng chỉ A, B, C</a></li>
                        <li class="nav_cat_73 hidden"><a cid="187" href="https://123doc.org/doc-cat/187-tong-hop.htm" onmousemove="showNavCat(this, 2)">Tổng hợp</a></li>
                        <li class="nav_cat_44 hidden"><a cid="45" href="https://123doc.org/doc-cat/45-quan-ly-nha-nuoc.htm" onmousemove="showNavCat(this, 2)">Quản lý nhà nước</a></li>
                        <li class="nav_cat_44 hidden"><a cid="46" href="https://123doc.org/doc-cat/46-tieu-chuan-qui-chuan.htm" onmousemove="showNavCat(this, 2)">Tiêu chuẩn - Qui chuẩn</a></li>
                        <li class="nav_cat_44 hidden"><a cid="47" href="https://123doc.org/doc-cat/47-quan-ly-du-an.htm" onmousemove="showNavCat(this, 2)">Quản lý dự án</a></li>
                        <li class="nav_cat_44 hidden"><a cid="48" href="https://123doc.org/doc-cat/48-quy-hoach-do-thi.htm" onmousemove="showNavCat(this, 2)">Quy hoạch - Đô thị</a></li>
                        <li class="nav_cat_35 hidden"><a cid="37" href="https://123doc.org/doc-cat/37-quan-tri-kinh-doanh.htm" onmousemove="showNavCat(this, 2)">Quản trị kinh doanh</a></li>
                        <li class="nav_cat_35 hidden"><a cid="36" href="https://123doc.org/doc-cat/36-internet-marketing.htm" onmousemove="showNavCat(this, 2)">Internet Marketing</a></li>
                        <li class="nav_cat_35 hidden"><a cid="38" href="https://123doc.org/doc-cat/38-ke-hoach-kinh-doanh.htm" onmousemove="showNavCat(this, 2)">Kế hoạch kinh doanh</a></li>
                        <li class="nav_cat_35 hidden"><a cid="41" href="https://123doc.org/doc-cat/41-thuong-mai-dien-tu.htm" onmousemove="showNavCat(this, 2)">Thương mại điện tử</a></li>
                        <li class="nav_cat_35 hidden"><a cid="39" href="https://123doc.org/doc-cat/39-tiep-thi-ban-hang.htm" onmousemove="showNavCat(this, 2)">Tiếp thị - Bán hàng</a></li>
                        <li class="nav_cat_35 hidden"><a cid="42" href="https://123doc.org/doc-cat/42-pr-truyen-thong.htm" onmousemove="showNavCat(this, 2)">PR - Truyền thông</a></li>
                        <li class="nav_cat_35 hidden"><a cid="40" href="https://123doc.org/doc-cat/40-ky-nang-ban-hang.htm" onmousemove="showNavCat(this, 2)">Kỹ năng bán hàng</a></li>
                        <li class="nav_cat_35 hidden"><a cid="43" href="https://123doc.org/doc-cat/43-to-chuc-su-kien.htm" onmousemove="showNavCat(this, 2)">Tổ chức sự kiện</a></li>
                        <li class="nav_cat_49 hidden"><a cid="51" href="https://123doc.org/doc-cat/51-bieu-mau.htm" onmousemove="showNavCat(this, 2)">Biểu mẫu</a></li>
                        <li class="nav_cat_49 hidden"><a cid="52" href="https://123doc.org/doc-cat/52-don-tu.htm" onmousemove="showNavCat(this, 2)">Đơn từ</a></li>
                        <li class="nav_cat_49 hidden"><a cid="50" href="https://123doc.org/doc-cat/50-thu-tuc-hanh-chinh.htm" onmousemove="showNavCat(this, 2)">Thủ tục hành chính</a></li>
                        <li class="nav_cat_49 hidden"><a cid="53" href="https://123doc.org/doc-cat/53-hop-dong.htm" onmousemove="showNavCat(this, 2)">Hợp đồng</a></li>
                        <li class="nav_cat_62 hidden"><a cid="68" href="https://123doc.org/doc-cat/68-ky-thuat-lap-trinh.htm" onmousemove="showNavCat(this, 2)">Kỹ thuật lập trình</a></li>
                        <li class="nav_cat_62 hidden"><a cid="65" href="https://123doc.org/doc-cat/65-quan-tri-mang.htm" onmousemove="showNavCat(this, 2)">Quản trị mạng</a></li>
                        <li class="nav_cat_62 hidden"><a cid="72" href="https://123doc.org/doc-cat/72-thiet-ke-do-hoa-flash.htm" onmousemove="showNavCat(this, 2)">Thiết kế - Đồ họa - Flash</a></li>
                        <li class="nav_cat_62 hidden"><a cid="64" href="https://123doc.org/doc-cat/64-he-dieu-hanh.htm" onmousemove="showNavCat(this, 2)">Hệ điều hành</a></li>
                        <li class="nav_cat_62 hidden"><a cid="67" href="https://123doc.org/doc-cat/67-co-so-du-lieu.htm" onmousemove="showNavCat(this, 2)">Cơ sở dữ liệu</a></li>
                        <li class="nav_cat_62 hidden"><a cid="70" href="https://123doc.org/doc-cat/70-tin-hoc-van-phong.htm" onmousemove="showNavCat(this, 2)">Tin học văn phòng</a></li>
                        <li class="nav_cat_62 hidden"><a cid="71" href="https://123doc.org/doc-cat/71-an-ninh-bao-mat.htm" onmousemove="showNavCat(this, 2)">An ninh - Bảo mật</a></li>
                        <li class="nav_cat_62 hidden"><a cid="66" href="https://123doc.org/doc-cat/66-quan-tri-web.htm" onmousemove="showNavCat(this, 2)">Quản trị Web</a></li>
                        <li class="nav_cat_62 hidden"><a cid="69" href="https://123doc.org/doc-cat/69-chung-chi-quoc-te.htm" onmousemove="showNavCat(this, 2)">Chứng chỉ quốc tế</a></li>
                        <li class="nav_cat_62 hidden"><a cid="63" href="https://123doc.org/doc-cat/63-phan-cung.htm" onmousemove="showNavCat(this, 2)">Phần cứng</a></li>
                        <li class="nav_cat_186 hidden"><a cid="188" href="https://123doc.org/doc-cat/188-tai-lieu-khac.htm" onmousemove="showNavCat(this, 2)">Tài liệu khác</a></li>
                        <li class="nav_cat_85 hidden"><a cid="88" href="https://123doc.org/doc-cat/88-co-khi-che-tao-may.htm" onmousemove="showNavCat(this, 2)">Cơ khí - Chế tạo máy</a></li>
                        <li class="nav_cat_85 hidden"><a cid="86" href="https://123doc.org/doc-cat/86-dien-dien-tu.htm" onmousemove="showNavCat(this, 2)">Điện - Điện tử</a></li>
                        <li class="nav_cat_85 hidden"><a cid="87" href="https://123doc.org/doc-cat/87-ki-thuat-vien-thong.htm" onmousemove="showNavCat(this, 2)">Kĩ thuật Viễn thông</a></li>
                        <li class="nav_cat_85 hidden"><a cid="90" href="https://123doc.org/doc-cat/90-kien-truc-xay-dung.htm" onmousemove="showNavCat(this, 2)">Kiến trúc - Xây dựng</a></li>
                        <li class="nav_cat_85 hidden"><a cid="91" href="https://123doc.org/doc-cat/91-tu-dong-hoa.htm" onmousemove="showNavCat(this, 2)">Tự động hóa</a></li>
                        <li class="nav_cat_85 hidden"><a cid="89" href="https://123doc.org/doc-cat/89-hoa-hoc-dau-khi.htm" onmousemove="showNavCat(this, 2)">Hóa học - Dầu khí</a></li>
                        <li class="nav_cat_85 hidden"><a cid="92" href="https://123doc.org/doc-cat/92-nang-luong.htm" onmousemove="showNavCat(this, 2)">Năng lượng</a></li>
                        <li class="nav_cat_93 hidden"><a cid="94" href="https://123doc.org/doc-cat/94-toan-hoc.htm" onmousemove="showNavCat(this, 2)">Toán học</a></li>
                        <li class="nav_cat_93 hidden"><a cid="95" href="https://123doc.org/doc-cat/95-vat-ly.htm" onmousemove="showNavCat(this, 2)">Vật lý</a></li>
                        <li class="nav_cat_93 hidden"><a cid="96" href="https://123doc.org/doc-cat/96-hoa-hoc-dau-khi.htm" onmousemove="showNavCat(this, 2)">Hóa học - Dầu khí</a></li>
                        <li class="nav_cat_93 hidden"><a cid="97" href="https://123doc.org/doc-cat/97-sinh-hoc.htm" onmousemove="showNavCat(this, 2)">Sinh học</a></li>
                        <li class="nav_cat_93 hidden"><a cid="98" href="https://123doc.org/doc-cat/98-moi-truong.htm" onmousemove="showNavCat(this, 2)">Môi trường</a></li>
                        <li class="nav_cat_121 hidden"><a cid="126" href="https://123doc.org/doc-cat/126-y-hoc-thuong-thuc.htm" onmousemove="showNavCat(this, 2)">Y học thưởng thức</a></li>
                        <li class="nav_cat_121 hidden"><a cid="122" href="https://123doc.org/doc-cat/122-suc-khoe-tre-em.htm" onmousemove="showNavCat(this, 2)">Sức khỏe trẻ em</a></li>
                        <li class="nav_cat_121 hidden"><a cid="123" href="https://123doc.org/doc-cat/123-suc-khoe-phu-nu.htm" onmousemove="showNavCat(this, 2)">Sức khỏe phụ nữ</a></li>
                        <li class="nav_cat_121 hidden"><a cid="124" href="https://123doc.org/doc-cat/124-suc-khoe-nguoi-cao-tuoi.htm" onmousemove="showNavCat(this, 2)">Sức khỏe người cao tuổi</a></li>
                        <li class="nav_cat_121 hidden"><a cid="125" href="https://123doc.org/doc-cat/125-suc-khoe-gioi-tinh.htm" onmousemove="showNavCat(this, 2)">Sức khỏe giới tính</a></li>
                        <li class="nav_cat_154 hidden"><a cid="155" href="https://123doc.org/doc-cat/155-tam-ly-nghe-thuat-song.htm" onmousemove="showNavCat(this, 2)">Tâm lý - Nghệ thuật sống</a></li>
                        <li class="nav_cat_154 hidden"><a cid="162" href="https://123doc.org/doc-cat/162-ky-nang-tu-duy.htm" onmousemove="showNavCat(this, 2)">Kỹ năng tư duy</a></li>
                        <li class="nav_cat_154 hidden"><a cid="158" href="https://123doc.org/doc-cat/158-ky-nang-quan-ly.htm" onmousemove="showNavCat(this, 2)">Kỹ năng quản lý</a></li>
                        <li class="nav_cat_154 hidden"><a cid="156" href="https://123doc.org/doc-cat/156-ky-nang-giao-tiep.htm" onmousemove="showNavCat(this, 2)">Kỹ năng giao tiếp</a></li>
                        <li class="nav_cat_154 hidden"><a cid="157" href="https://123doc.org/doc-cat/157-ky-nang-thuyet-trinh.htm" onmousemove="showNavCat(this, 2)">Kỹ năng thuyết trình</a></li>
                        <li class="nav_cat_154 hidden"><a cid="159" href="https://123doc.org/doc-cat/159-ky-nang-lanh-dao.htm" onmousemove="showNavCat(this, 2)">Kỹ năng lãnh đạo</a></li>
                        <li class="nav_cat_154 hidden"><a cid="160" href="https://123doc.org/doc-cat/160-ky-nang-phong-van.htm" onmousemove="showNavCat(this, 2)">Kỹ năng phỏng vấn </a></li>
                        <li class="nav_cat_154 hidden"><a cid="164" href="https://123doc.org/doc-cat/164-ky-nang-dam-phan.htm" onmousemove="showNavCat(this, 2)">Kỹ năng đàm phán</a></li>
                        <li class="nav_cat_154 hidden"><a cid="163" href="https://123doc.org/doc-cat/163-ky-nang-to-chuc.htm" onmousemove="showNavCat(this, 2)">Kỹ năng tổ chức</a></li>
                        <li class="nav_cat_154 hidden"><a cid="161" href="https://123doc.org/doc-cat/161-ky-nang-lam-viec-nhom.htm" onmousemove="showNavCat(this, 2)">Kỹ năng làm việc nhóm</a></li>
                        <li class="nav_cat_165 hidden"><a cid="166" href="https://123doc.org/doc-cat/166-nong-nghiep.htm" onmousemove="showNavCat(this, 2)">Nông nghiệp</a></li>
                        <li class="nav_cat_165 hidden"><a cid="167" href="https://123doc.org/doc-cat/167-lam-nghiep.htm" onmousemove="showNavCat(this, 2)">Lâm nghiệp</a></li>
                        <li class="nav_cat_165 hidden"><a cid="168" href="https://123doc.org/doc-cat/168-ngu-nghiep.htm" onmousemove="showNavCat(this, 2)">Ngư nghiệp</a></li>
                        <li class="nav_cat_169"><a cid="184" href="https://123doc.org/doc-cat/184-thac-si-cao-hoc.htm" onmousemove="showNavCat(this, 2)">Thạc sĩ - Cao học</a></li>
                        <li class="nav_cat_169"><a cid="282" href="https://123doc.org/doc-cat/282-kinh-te-quan-ly.htm" onmousemove="showNavCat(this, 2)">Kinh tế - Quản lý</a></li>
                        <li class="nav_cat_169"><a cid="181" href="https://123doc.org/doc-cat/181-khoa-hoc-tu-nhien.htm" onmousemove="showNavCat(this, 2)">Khoa học tự nhiên</a></li>
                        <li class="nav_cat_169"><a cid="173" href="https://123doc.org/doc-cat/173-kinh-te-thuong-mai.htm" onmousemove="showNavCat(this, 2)">Kinh tế - Thương mại</a></li>
                        <li class="nav_cat_169"><a cid="283" href="https://123doc.org/doc-cat/283-ly-luan-chinh-tri.htm" onmousemove="showNavCat(this, 2)">Lý luận chính trị</a></li>
                        <li class="nav_cat_169"><a cid="174" href="https://123doc.org/doc-cat/174-cong-nghe-thong-tin.htm" onmousemove="showNavCat(this, 2)">Công nghệ thông tin</a></li>
                        <li class="nav_cat_169"><a cid="346" href="https://123doc.org/doc-cat/346-ky-thuat.htm" onmousemove="showNavCat(this, 2)">Kỹ thuật</a></li>
                        <li class="nav_cat_169"><a cid="182" href="https://123doc.org/doc-cat/182-nong-lam-ngu.htm" onmousemove="showNavCat(this, 2)">Nông - Lâm - Ngư</a></li>
                        <li class="nav_cat_169"><a cid="178" href="https://123doc.org/doc-cat/178-cong-nghe-moi-truong.htm" onmousemove="showNavCat(this, 2)">Công nghệ - Môi trường</a></li>
                        <li class="nav_cat_169"><a cid="179" href="https://123doc.org/doc-cat/179-y-khoa-duoc.htm" onmousemove="showNavCat(this, 2)">Y khoa - Dược</a></li>
                        <li class="nav_cat_169"><a cid="180" href="https://123doc.org/doc-cat/180-khoa-hoc-xa-hoi.htm" onmousemove="showNavCat(this, 2)">Khoa học xã hội</a></li>
                        <li class="nav_cat_169"><a cid="183" href="https://123doc.org/doc-cat/183-bao-cao-khoa-hoc.htm" onmousemove="showNavCat(this, 2)">Báo cáo khoa học</a></li>
                        <li class="nav_cat_169"><a cid="185" href="https://123doc.org/doc-cat/185-tien-si.htm" onmousemove="showNavCat(this, 2)">Tiến sĩ</a></li>
                        <li class="nav_cat_284 hidden"><a cid="285" href="https://123doc.org/doc-cat/285-hoa-hoc.htm" onmousemove="showNavCat(this, 2)">Hóa học</a></li>
                        <li class="nav_cat_284 hidden"><a cid="734" href="https://123doc.org/doc-cat/734-van-mau.htm" onmousemove="showNavCat(this, 2)">Văn Mẫu</a></li>
                        <li class="nav_cat_284 hidden"><a cid="286" href="https://123doc.org/doc-cat/286-ngu-van.htm" onmousemove="showNavCat(this, 2)">Ngữ văn</a></li>
                        <li class="nav_cat_284 hidden"><a cid="287" href="https://123doc.org/doc-cat/287-vat-ly.htm" onmousemove="showNavCat(this, 2)">Vật lý</a></li>
                        <li class="nav_cat_284 hidden"><a cid="288" href="https://123doc.org/doc-cat/288-toan-hoc.htm" onmousemove="showNavCat(this, 2)">Toán học</a></li>
                        <li class="nav_cat_284 hidden"><a cid="289" href="https://123doc.org/doc-cat/289-sinh-hoc.htm" onmousemove="showNavCat(this, 2)">Sinh học</a></li>
                        <li class="nav_cat_284 hidden"><a cid="290" href="https://123doc.org/doc-cat/290-lich-su.htm" onmousemove="showNavCat(this, 2)">Lịch sử</a></li>
                        <li class="nav_cat_284 hidden"><a cid="291" href="https://123doc.org/doc-cat/291-dia-ly.htm" onmousemove="showNavCat(this, 2)">Địa lý</a></li>
                        <li class="nav_cat_284 hidden"><a cid="292" href="https://123doc.org/doc-cat/292-gdcd-gdngll.htm" onmousemove="showNavCat(this, 2)">GDCD-GDNGLL</a></li>
                        <li class="nav_cat_284 hidden"><a cid="293" href="https://123doc.org/doc-cat/293-am-nhac.htm" onmousemove="showNavCat(this, 2)">Âm nhạc</a></li>
                        <li class="nav_cat_284 hidden"><a cid="294" href="https://123doc.org/doc-cat/294-my-thuat.htm" onmousemove="showNavCat(this, 2)">Mỹ thuật</a></li>
                        <li class="nav_cat_284 hidden"><a cid="295" href="https://123doc.org/doc-cat/295-the-duc.htm" onmousemove="showNavCat(this, 2)">Thể dục</a></li>
                        <li class="nav_cat_284 hidden"><a cid="296" href="https://123doc.org/doc-cat/296-cong-nghe.htm" onmousemove="showNavCat(this, 2)">Công nghệ</a></li>
                        <li class="nav_cat_284 hidden"><a cid="297" href="https://123doc.org/doc-cat/297-tin-hoc.htm" onmousemove="showNavCat(this, 2)">Tin học</a></li>
                        <li class="nav_cat_284 hidden"><a cid="298" href="https://123doc.org/doc-cat/298-tieng-anh.htm" onmousemove="showNavCat(this, 2)">Tiếng anh</a></li>
                        <li class="nav_cat_284 hidden"><a cid="299" href="https://123doc.org/doc-cat/299-giao-duc-huong-nhiep.htm" onmousemove="showNavCat(this, 2)">Giáo dục hướng nhiệp</a></li>
                        <li class="nav_cat_284 hidden"><a cid="300" href="https://123doc.org/doc-cat/300-mam-non-mau-giao.htm" onmousemove="showNavCat(this, 2)">Mầm non - Mẫu giáo</a></li>
                        <li class="nav_cat_284 hidden"><a cid="301" href="https://123doc.org/doc-cat/301-tieu-hoc.htm" onmousemove="showNavCat(this, 2)">Tiểu học</a></li>
                        <li class="nav_cat_284 hidden"><a cid="302" href="https://123doc.org/doc-cat/302-cao-dang-dai-hoc.htm" onmousemove="showNavCat(this, 2)">Cao đẳng - Đại học</a></li>
                        <li class="nav_cat_284 hidden"><a cid="303" href="https://123doc.org/doc-cat/303-tu-lieu-khac.htm" onmousemove="showNavCat(this, 2)">Tư liệu khác</a></li>
                        <li class="nav_cat_672 hidden"><a cid="673" href="https://123doc.org/doc-cat/673-mau-slide-template.htm" onmousemove="showNavCat(this, 2)">Mẫu Slide - Template</a></li>
                        <li class="nav_cat_672 hidden"><a cid="674" href="https://123doc.org/doc-cat/674-hinh-nen-background.htm" onmousemove="showNavCat(this, 2)">Hình Nền - Background</a></li>
                        <li class="nav_cat_672 hidden"><a cid="687" href="https://123doc.org/doc-cat/687-so-do-diagram.htm" onmousemove="showNavCat(this, 2)">Sơ Đồ - Diagram</a></li>
                        <li class="nav_cat_672 hidden"><a cid="692" href="https://123doc.org/doc-cat/692-bieu-do-chart.htm" onmousemove="showNavCat(this, 2)">Biểu Đồ - Chart</a></li>
                        <li class="nav_cat_672 hidden"><a cid="698" href="https://123doc.org/doc-cat/698-hinh-ve-shape.htm" onmousemove="showNavCat(this, 2)">Hình Vẽ - Shape</a></li>
                        <li class="nav_cat_672 hidden"><a cid="704" href="https://123doc.org/doc-cat/704-van-ban-text.htm" onmousemove="showNavCat(this, 2)">Văn Bản - Text</a></li>
                        <li class="nav_cat_672 hidden"><a cid="712" href="https://123doc.org/doc-cat/712-bieu-tuong-clipart.htm" onmousemove="showNavCat(this, 2)">Biểu Tượng - Clipart</a></li>
                        <li class="nav_cat_672 hidden"><a cid="717" href="https://123doc.org/doc-cat/717-phan-tich-analysis.htm" onmousemove="showNavCat(this, 2)">Phân tích - Analysis</a></li>
                        <li class="nav_cat_672 hidden"><a cid="718" href="https://123doc.org/doc-cat/718-ke-hoach-planning.htm" onmousemove="showNavCat(this, 2)">Kế Hoạch - Planning</a></li>
                        <li class="nav_cat_672 hidden"><a cid="723" href="https://123doc.org/doc-cat/723-hinh-minh-hoa-image.htm" onmousemove="showNavCat(this, 2)">Hình Minh Họa - Image</a></li>
                        <li class="nav_cat_99 hidden"><a cid="1810" href="https://123doc.org/doc-cat/1810-de-thi.htm" onmousemove="showNavCat(this, 2)">Đề thi</a></li>
                        <li class="nav_cat_99 hidden"><a cid="100" href="https://123doc.org/doc-cat/100-mam-non-tieu-hoc.htm" onmousemove="showNavCat(this, 2)">Mầm non - Tiểu học</a></li>
                        <li class="nav_cat_99 hidden"><a cid="103" href="https://123doc.org/doc-cat/103-trung-hoc-co-so-pho-thong.htm" onmousemove="showNavCat(this, 2)">Trung học cơ sở - phổ thông</a></li>
                        <li class="nav_cat_99 hidden"><a cid="102" href="https://123doc.org/doc-cat/102-on-thi-dai-hoc-cao-dang.htm" onmousemove="showNavCat(this, 2)">Ôn thi Đại học - Cao đẳng</a></li>
                        <li class="nav_cat_99 hidden"><a cid="101" href="https://123doc.org/doc-cat/101-cao-dang-dai-hoc.htm" onmousemove="showNavCat(this, 2)">Cao đẳng - Đại học</a></li>
                        <li class="nav_cat_104 hidden"><a cid="109" href="https://123doc.org/doc-cat/109-am-nhac.htm" onmousemove="showNavCat(this, 2)">Âm nhạc</a></li>
                        <li class="nav_cat_104 hidden"><a cid="106" href="https://123doc.org/doc-cat/106-am-thuc.htm" onmousemove="showNavCat(this, 2)">Ẩm thực</a></li>
                        <li class="nav_cat_104 hidden"><a cid="107" href="https://123doc.org/doc-cat/107-kheo-tay-hay-lam.htm" onmousemove="showNavCat(this, 2)">Khéo tay hay làm</a></li>
                        <li class="nav_cat_104 hidden"><a cid="112" href="https://123doc.org/doc-cat/112-chup-anh-quay-phim.htm" onmousemove="showNavCat(this, 2)">Chụp ảnh - Quay phim</a></li>
                        <li class="nav_cat_104 hidden"><a cid="110" href="https://123doc.org/doc-cat/110-my-thuat.htm" onmousemove="showNavCat(this, 2)">Mỹ thuật</a></li>
                        <li class="nav_cat_104 hidden"><a cid="111" href="https://123doc.org/doc-cat/111-dieu-khac-hoi-hoa.htm" onmousemove="showNavCat(this, 2)">Điêu khắc - Hội họa</a></li>
                        <li class="nav_cat_104 hidden"><a cid="105" href="https://123doc.org/doc-cat/105-thoi-trang-lam-dep.htm" onmousemove="showNavCat(this, 2)">Thời trang - Làm đẹp</a></li>
                        <li class="nav_cat_104 hidden"><a cid="108" href="https://123doc.org/doc-cat/108-san-khau-dien-anh.htm" onmousemove="showNavCat(this, 2)">Sân khấu điện ảnh</a></li>
                        <li class="nav_cat_104 hidden"><a cid="119" href="https://123doc.org/doc-cat/119-du-lich.htm" onmousemove="showNavCat(this, 2)">Du lịch</a></li>
                        <li class="nav_cat_318 hidden"><a cid="323" href="https://123doc.org/doc-cat/323-chung-khoan.htm" onmousemove="showNavCat(this, 2)">Chứng khoán</a></li>
                      </ul>
                      <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 280px;"></div>
                      <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                    </div>
                  </div>
                  <div class="sub_nav_2" data-rel="2">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 200px; height: 280px;">
                      <ul style="overflow: hidden; width: 200px; height: 280px;">
                        <li class="nav_cat_100 hidden"><a cid="307" href="https://123doc.org/doc-cat/307-mam-non.htm" onmousemove="showNavCat(this, 3)">Mầm non</a></li>
                        <li class="nav_cat_100 hidden"><a cid="361" href="https://123doc.org/doc-cat/361-lop-1.htm" onmousemove="showNavCat(this, 3)">Lớp 1</a></li>
                        <li class="nav_cat_100 hidden"><a cid="362" href="https://123doc.org/doc-cat/362-lop-2.htm" onmousemove="showNavCat(this, 3)">Lớp 2</a></li>
                        <li class="nav_cat_100 hidden"><a cid="363" href="https://123doc.org/doc-cat/363-lop-3.htm" onmousemove="showNavCat(this, 3)">Lớp 3</a></li>
                        <li class="nav_cat_100 hidden"><a cid="364" href="https://123doc.org/doc-cat/364-lop-4.htm" onmousemove="showNavCat(this, 3)">Lớp 4</a></li>
                        <li class="nav_cat_100 hidden"><a cid="365" href="https://123doc.org/doc-cat/365-lop-5.htm" onmousemove="showNavCat(this, 3)">Lớp 5</a></li>
                        <li class="nav_cat_184 hidden"><a cid="308" href="https://123doc.org/doc-cat/308-kinh-te.htm" onmousemove="showNavCat(this, 3)">Kinh tế</a></li>
                        <li class="nav_cat_184 hidden"><a cid="309" href="https://123doc.org/doc-cat/309-khoa-hoc-xa-hoi.htm" onmousemove="showNavCat(this, 3)">Khoa học xã hội</a></li>
                        <li class="nav_cat_184 hidden"><a cid="310" href="https://123doc.org/doc-cat/310-khoa-hoc-tu-nhien.htm" onmousemove="showNavCat(this, 3)">Khoa học tự nhiên</a></li>
                        <li class="nav_cat_184 hidden"><a cid="311" href="https://123doc.org/doc-cat/311-cong-nghe-thong-tin.htm" onmousemove="showNavCat(this, 3)">Công nghệ thông tin</a></li>
                        <li class="nav_cat_184 hidden"><a cid="312" href="https://123doc.org/doc-cat/312-ky-thuat.htm" onmousemove="showNavCat(this, 3)">Kỹ thuật</a></li>
                        <li class="nav_cat_184 hidden"><a cid="313" href="https://123doc.org/doc-cat/313-nong-lam-ngu.htm" onmousemove="showNavCat(this, 3)">Nông - Lâm - Ngư</a></li>
                        <li class="nav_cat_184 hidden"><a cid="314" href="https://123doc.org/doc-cat/314-kien-truc-xay-dung.htm" onmousemove="showNavCat(this, 3)">Kiến trúc - Xây dựng</a></li>
                        <li class="nav_cat_184 hidden"><a cid="315" href="https://123doc.org/doc-cat/315-luat.htm" onmousemove="showNavCat(this, 3)">Luật</a></li>
                        <li class="nav_cat_184 hidden"><a cid="316" href="https://123doc.org/doc-cat/316-su-pham.htm" onmousemove="showNavCat(this, 3)">Sư phạm</a></li>
                        <li class="nav_cat_184 hidden"><a cid="317" href="https://123doc.org/doc-cat/317-y-duoc-sinh-hoc.htm" onmousemove="showNavCat(this, 3)">Y dược - Sinh học</a></li>
                        <li class="nav_cat_282 hidden"><a cid="319" href="https://123doc.org/doc-cat/319-dich-vu-du-lich.htm" onmousemove="showNavCat(this, 3)">Dịch vụ - Du lịch</a></li>
                        <li class="nav_cat_282 hidden"><a cid="320" href="https://123doc.org/doc-cat/320-bat-dong-san.htm" onmousemove="showNavCat(this, 3)">Bất động sản</a></li>
                        <li class="nav_cat_282 hidden"><a cid="170" href="https://123doc.org/doc-cat/170-tai-chinh-ngan-hang.htm" onmousemove="showNavCat(this, 3)">Tài chính - Ngân hàng</a></li>
                        <li class="nav_cat_282 hidden"><a cid="172" href="https://123doc.org/doc-cat/172-quan-tri-kinh-doanh.htm" onmousemove="showNavCat(this, 3)">Quản trị kinh doanh</a></li>
                        <li class="nav_cat_282 hidden"><a cid="171" href="https://123doc.org/doc-cat/171-ke-toan.htm" onmousemove="showNavCat(this, 3)">Kế toán</a></li>
                        <li class="nav_cat_282 hidden"><a cid="321" href="https://123doc.org/doc-cat/321-kiem-toan.htm" onmousemove="showNavCat(this, 3)">Kiểm toán</a></li>
                        <li class="nav_cat_282 hidden"><a cid="322" href="https://123doc.org/doc-cat/322-xuat-nhap-khau.htm" onmousemove="showNavCat(this, 3)">Xuất nhập khẩu</a></li>
                        <li class="nav_cat_282 hidden"><a cid="325" href="https://123doc.org/doc-cat/325-chung-khoan.htm" onmousemove="showNavCat(this, 3)">Chứng khoán</a></li>
                        <li class="nav_cat_282 hidden"><a cid="324" href="https://123doc.org/doc-cat/324-tai-chinh-thue.htm" onmousemove="showNavCat(this, 3)">Tài chính thuế</a></li>
                        <li class="nav_cat_282 hidden"><a cid="326" href="https://123doc.org/doc-cat/326-marketing.htm" onmousemove="showNavCat(this, 3)">Marketing</a></li>
                        <li class="nav_cat_282 hidden"><a cid="327" href="https://123doc.org/doc-cat/327-bao-hiem.htm" onmousemove="showNavCat(this, 3)">Bảo hiểm</a></li>
                        <li class="nav_cat_282 hidden"><a cid="328" href="https://123doc.org/doc-cat/328-dinh-gia-dau-thau.htm" onmousemove="showNavCat(this, 3)">Định giá - Đấu thầu</a></li>
                        <li class="nav_cat_181 hidden"><a cid="329" href="https://123doc.org/doc-cat/329-toan-hoc.htm" onmousemove="showNavCat(this, 3)">Toán học</a></li>
                        <li class="nav_cat_181 hidden"><a cid="330" href="https://123doc.org/doc-cat/330-vat-ly.htm" onmousemove="showNavCat(this, 3)">Vật lý</a></li>
                        <li class="nav_cat_181 hidden"><a cid="331" href="https://123doc.org/doc-cat/331-hoa-hoc.htm" onmousemove="showNavCat(this, 3)">Hóa học</a></li>
                        <li class="nav_cat_181 hidden"><a cid="332" href="https://123doc.org/doc-cat/332-sinh-hoc.htm" onmousemove="showNavCat(this, 3)">Sinh học</a></li>
                        <li class="nav_cat_181 hidden"><a cid="333" href="https://123doc.org/doc-cat/333-dia-ly-dia-chat.htm" onmousemove="showNavCat(this, 3)">Địa lý - Địa chất</a></li>
                        <li class="nav_cat_283 hidden"><a cid="334" href="https://123doc.org/doc-cat/334-triet-hoc-mac-lenin.htm" onmousemove="showNavCat(this, 3)">Triết học Mác - Lênin</a></li>
                        <li class="nav_cat_283 hidden"><a cid="335" href="https://123doc.org/doc-cat/335-duong-loi-cach-mang-dang-cong-san-viet-nam.htm" onmousemove="showNavCat(this, 3)">Đường lối cách mạng Đảng cộng sản Việt Nam</a></li>
                        <li class="nav_cat_283 hidden"><a cid="336" href="https://123doc.org/doc-cat/336-kinh-te-chinh-tri.htm" onmousemove="showNavCat(this, 3)">Kinh tế chính trị</a></li>
                        <li class="nav_cat_283 hidden"><a cid="337" href="https://123doc.org/doc-cat/337-chu-nghia-xa-hoi-khoa-hoc.htm" onmousemove="showNavCat(this, 3)">Chủ nghĩa xã hội khoa học</a></li>
                        <li class="nav_cat_283 hidden"><a cid="338" href="https://123doc.org/doc-cat/338-tu-tuong-ho-chi-minh.htm" onmousemove="showNavCat(this, 3)">Tư tưởng Hồ Chí Minh</a></li>
                        <li class="nav_cat_174 hidden"><a cid="339" href="https://123doc.org/doc-cat/339-quan-tri-mang.htm" onmousemove="showNavCat(this, 3)">Quản trị mạng</a></li>
                        <li class="nav_cat_174 hidden"><a cid="340" href="https://123doc.org/doc-cat/340-lap-trinh.htm" onmousemove="showNavCat(this, 3)">Lập trình</a></li>
                        <li class="nav_cat_174 hidden"><a cid="341" href="https://123doc.org/doc-cat/341-do-hoa.htm" onmousemove="showNavCat(this, 3)">Đồ họa</a></li>
                        <li class="nav_cat_174 hidden"><a cid="342" href="https://123doc.org/doc-cat/342-web.htm" onmousemove="showNavCat(this, 3)">Web</a></li>
                        <li class="nav_cat_174 hidden"><a cid="343" href="https://123doc.org/doc-cat/343-he-thong-thong-tin.htm" onmousemove="showNavCat(this, 3)">Hệ thống thông tin</a></li>
                        <li class="nav_cat_174 hidden"><a cid="344" href="https://123doc.org/doc-cat/344-thuong-mai-dien-tu.htm" onmousemove="showNavCat(this, 3)">Thương mại điện tử</a></li>
                        <li class="nav_cat_346 hidden"><a cid="347" href="https://123doc.org/doc-cat/347-giao-thong-van-tai.htm" onmousemove="showNavCat(this, 3)">Giao thông - Vận tải</a></li>
                        <li class="nav_cat_346 hidden"><a cid="175" href="https://123doc.org/doc-cat/175-dien-dien-tu-vien-thong.htm" onmousemove="showNavCat(this, 3)">Điện - Điện tử - Viễn thông</a></li>
                        <li class="nav_cat_346 hidden"><a cid="176" href="https://123doc.org/doc-cat/176-co-khi-vat-lieu.htm" onmousemove="showNavCat(this, 3)">Cơ khí - Vật liệu</a></li>
                        <li class="nav_cat_346 hidden"><a cid="177" href="https://123doc.org/doc-cat/177-kien-truc-xay-dung.htm" onmousemove="showNavCat(this, 3)">Kiến trúc - Xây dựng</a></li>
                        <li class="nav_cat_346 hidden"><a cid="348" href="https://123doc.org/doc-cat/348-hoa-dau.htm" onmousemove="showNavCat(this, 3)">Hóa dầu</a></li>
                        <li class="nav_cat_182 hidden"><a cid="349" href="https://123doc.org/doc-cat/349-lam-nghiep.htm" onmousemove="showNavCat(this, 3)">Lâm nghiệp</a></li>
                        <li class="nav_cat_182 hidden"><a cid="350" href="https://123doc.org/doc-cat/350-nong-hoc.htm" onmousemove="showNavCat(this, 3)">Nông học</a></li>
                        <li class="nav_cat_182 hidden"><a cid="351" href="https://123doc.org/doc-cat/351-chan-nuoi.htm" onmousemove="showNavCat(this, 3)">Chăn nuôi</a></li>
                        <li class="nav_cat_182 hidden"><a cid="352" href="https://123doc.org/doc-cat/352-thu-y.htm" onmousemove="showNavCat(this, 3)">Thú y</a></li>
                        <li class="nav_cat_182 hidden"><a cid="353" href="https://123doc.org/doc-cat/353-thuy-san.htm" onmousemove="showNavCat(this, 3)">Thủy sản</a></li>
                        <li class="nav_cat_182 hidden"><a cid="354" href="https://123doc.org/doc-cat/354-cong-nghe-thuc-pham.htm" onmousemove="showNavCat(this, 3)">Công nghệ thực phẩm</a></li>
                        <li class="nav_cat_103 hidden"><a cid="366" href="https://123doc.org/doc-cat/366-lop-6.htm" onmousemove="showNavCat(this, 3)">Lớp 6</a></li>
                        <li class="nav_cat_103 hidden"><a cid="369" href="https://123doc.org/doc-cat/369-lop-7.htm" onmousemove="showNavCat(this, 3)">Lớp 7</a></li>
                        <li class="nav_cat_103 hidden"><a cid="370" href="https://123doc.org/doc-cat/370-lop-8.htm" onmousemove="showNavCat(this, 3)">Lớp 8</a></li>
                        <li class="nav_cat_103 hidden"><a cid="371" href="https://123doc.org/doc-cat/371-lop-9.htm" onmousemove="showNavCat(this, 3)">Lớp 9</a></li>
                        <li class="nav_cat_103 hidden"><a cid="372" href="https://123doc.org/doc-cat/372-lop-10.htm" onmousemove="showNavCat(this, 3)">Lớp 10</a></li>
                        <li class="nav_cat_103 hidden"><a cid="373" href="https://123doc.org/doc-cat/373-lop-11.htm" onmousemove="showNavCat(this, 3)">Lớp 11</a></li>
                        <li class="nav_cat_103 hidden"><a cid="374" href="https://123doc.org/doc-cat/374-lop-12.htm" onmousemove="showNavCat(this, 3)">Lớp 12</a></li>
                        <li class="nav_cat_102 hidden"><a cid="461" href="https://123doc.org/doc-cat/461-toan-hoc.htm" onmousemove="showNavCat(this, 3)">Toán học</a></li>
                        <li class="nav_cat_102 hidden"><a cid="462" href="https://123doc.org/doc-cat/462-vat-ly.htm" onmousemove="showNavCat(this, 3)">Vật lý</a></li>
                        <li class="nav_cat_102 hidden"><a cid="463" href="https://123doc.org/doc-cat/463-hoa-hoc.htm" onmousemove="showNavCat(this, 3)">Hóa học</a></li>
                        <li class="nav_cat_102 hidden"><a cid="464" href="https://123doc.org/doc-cat/464-sinh-hoc.htm" onmousemove="showNavCat(this, 3)">Sinh học</a></li>
                        <li class="nav_cat_102 hidden"><a cid="465" href="https://123doc.org/doc-cat/465-lich-su.htm" onmousemove="showNavCat(this, 3)">Lịch sử</a></li>
                        <li class="nav_cat_102 hidden"><a cid="466" href="https://123doc.org/doc-cat/466-dia-ly.htm" onmousemove="showNavCat(this, 3)">Địa lý</a></li>
                        <li class="nav_cat_102 hidden"><a cid="467" href="https://123doc.org/doc-cat/467-van.htm" onmousemove="showNavCat(this, 3)">Văn</a></li>
                        <li class="nav_cat_102 hidden"><a cid="468" href="https://123doc.org/doc-cat/468-ngoai-ngu.htm" onmousemove="showNavCat(this, 3)">Ngoại ngữ</a></li>
                        <li class="nav_cat_101 hidden"><a cid="469" href="https://123doc.org/doc-cat/469-dai-cuong.htm" onmousemove="showNavCat(this, 3)">Đại cương</a></li>
                        <li class="nav_cat_101 hidden"><a cid="470" href="https://123doc.org/doc-cat/470-y-duoc.htm" onmousemove="showNavCat(this, 3)">Y - Dược</a></li>
                        <li class="nav_cat_101 hidden"><a cid="471" href="https://123doc.org/doc-cat/471-chuyen-nganh-kinh-te.htm" onmousemove="showNavCat(this, 3)">Chuyên ngành kinh tế</a></li>
                        <li class="nav_cat_101 hidden"><a cid="472" href="https://123doc.org/doc-cat/472-khoa-hoc-xa-hoi.htm" onmousemove="showNavCat(this, 3)">Khoa học xã hội</a></li>
                        <li class="nav_cat_101 hidden"><a cid="473" href="https://123doc.org/doc-cat/473-luat.htm" onmousemove="showNavCat(this, 3)">Luật</a></li>
                        <li class="nav_cat_101 hidden"><a cid="474" href="https://123doc.org/doc-cat/474-cong-nghe-thong-tin.htm" onmousemove="showNavCat(this, 3)">Công nghệ thông tin</a></li>
                        <li class="nav_cat_101 hidden"><a cid="475" href="https://123doc.org/doc-cat/475-su-pham.htm" onmousemove="showNavCat(this, 3)">Sư phạm</a></li>
                        <li class="nav_cat_101 hidden"><a cid="476" href="https://123doc.org/doc-cat/476-kien-truc-xay-dung.htm" onmousemove="showNavCat(this, 3)">Kiến trúc - Xây dựng</a></li>
                        <li class="nav_cat_101 hidden"><a cid="477" href="https://123doc.org/doc-cat/477-ky-thuat-cong-nghe.htm" onmousemove="showNavCat(this, 3)">Kỹ thuật - Công nghệ</a></li>
                        <li class="nav_cat_180 hidden"><a cid="603" href="https://123doc.org/doc-cat/603-quan-he-quoc-te.htm" onmousemove="showNavCat(this, 3)">Quan hệ quốc tế</a></li>
                        <li class="nav_cat_180 hidden"><a cid="604" href="https://123doc.org/doc-cat/604-giao-duc-hoc.htm" onmousemove="showNavCat(this, 3)">Giáo dục học</a></li>
                        <li class="nav_cat_180 hidden"><a cid="605" href="https://123doc.org/doc-cat/605-dong-phuong-hoc.htm" onmousemove="showNavCat(this, 3)">Đông phương học</a></li>
                        <li class="nav_cat_180 hidden"><a cid="606" href="https://123doc.org/doc-cat/606-viet-nam-hoc.htm" onmousemove="showNavCat(this, 3)">Việt Nam học</a></li>
                        <li class="nav_cat_180 hidden"><a cid="607" href="https://123doc.org/doc-cat/607-van-hoa-lich-su.htm" onmousemove="showNavCat(this, 3)">Văn hóa - Lịch sử</a></li>
                        <li class="nav_cat_180 hidden"><a cid="608" href="https://123doc.org/doc-cat/608-xa-hoi-hoc.htm" onmousemove="showNavCat(this, 3)">Xã hội học</a></li>
                        <li class="nav_cat_180 hidden"><a cid="609" href="https://123doc.org/doc-cat/609-bao-chi.htm" onmousemove="showNavCat(this, 3)">Báo chí</a></li>
                        <li class="nav_cat_180 hidden"><a cid="610" href="https://123doc.org/doc-cat/610-tam-ly-hoc.htm" onmousemove="showNavCat(this, 3)">Tâm lý học</a></li>
                        <li class="nav_cat_180 hidden"><a cid="611" href="https://123doc.org/doc-cat/611-van-hoc-ngon-ngu-hoc.htm" onmousemove="showNavCat(this, 3)">Văn học - Ngôn ngữ học</a></li>
                        <li class="nav_cat_674 hidden"><a cid="675" href="https://123doc.org/doc-cat/675-kinh-doanh-business.htm" onmousemove="showNavCat(this, 3)">Kinh Doanh - Business</a></li>
                        <li class="nav_cat_674 hidden"><a cid="676" href="https://123doc.org/doc-cat/676-giao-duc-education.htm" onmousemove="showNavCat(this, 3)">Giáo Dục - Education</a></li>
                        <li class="nav_cat_674 hidden"><a cid="677" href="https://123doc.org/doc-cat/677-tai-chinh-financial.htm" onmousemove="showNavCat(this, 3)">Tài Chính - Financial</a></li>
                        <li class="nav_cat_674 hidden"><a cid="678" href="https://123doc.org/doc-cat/678-san-pham-product.htm" onmousemove="showNavCat(this, 3)">Sản Phẩm - Product</a></li>
                        <li class="nav_cat_674 hidden"><a cid="679" href="https://123doc.org/doc-cat/679-dich-vu-service.htm" onmousemove="showNavCat(this, 3)">Dịch Vụ - Service</a></li>
                        <li class="nav_cat_674 hidden"><a cid="680" href="https://123doc.org/doc-cat/680-khoa-hoc-science.htm" onmousemove="showNavCat(this, 3)">Khoa Học - Science</a></li>
                        <li class="nav_cat_674 hidden"><a cid="681" href="https://123doc.org/doc-cat/681-cong-nghe-technology.htm" onmousemove="showNavCat(this, 3)">Công Nghệ - Technology</a></li>
                        <li class="nav_cat_674 hidden"><a cid="682" href="https://123doc.org/doc-cat/682-thien-nhien-natural.htm" onmousemove="showNavCat(this, 3)">Thiên Nhiên - Natural</a></li>
                        <li class="nav_cat_674 hidden"><a cid="683" href="https://123doc.org/doc-cat/683-con-nguoi-people.htm" onmousemove="showNavCat(this, 3)">Con Người - People</a></li>
                        <li class="nav_cat_674 hidden"><a cid="684" href="https://123doc.org/doc-cat/684-the-thao-sport.htm" onmousemove="showNavCat(this, 3)">Thể Thao - Sport</a></li>
                        <li class="nav_cat_674 hidden"><a cid="685" href="https://123doc.org/doc-cat/685-giai-tri-entertainment.htm" onmousemove="showNavCat(this, 3)">Giải Trí - Entertainment</a></li>
                        <li class="nav_cat_674 hidden"><a cid="686" href="https://123doc.org/doc-cat/686-truu-tuong-abstract.htm" onmousemove="showNavCat(this, 3)">Trừu Tượng - Abstract</a></li>
                        <li class="nav_cat_687 hidden"><a cid="688" href="https://123doc.org/doc-cat/688-tien-trinh-process.htm" onmousemove="showNavCat(this, 3)">Tiến Trình - Process</a></li>
                        <li class="nav_cat_687 hidden"><a cid="689" href="https://123doc.org/doc-cat/689-lap-hinh-puzzle.htm" onmousemove="showNavCat(this, 3)">Lắp Hình - Puzzle</a></li>
                        <li class="nav_cat_687 hidden"><a cid="690" href="https://123doc.org/doc-cat/690-giai-doan-stage.htm" onmousemove="showNavCat(this, 3)">Giai Đoạn - Stage</a></li>
                        <li class="nav_cat_687 hidden"><a cid="691" href="https://123doc.org/doc-cat/691-hinh-cay-tree.htm" onmousemove="showNavCat(this, 3)">Hình Cây - Tree</a></li>
                        <li class="nav_cat_692 hidden"><a cid="693" href="https://123doc.org/doc-cat/693-thanh-bar-chart.htm" onmousemove="showNavCat(this, 3)">Thanh - Bar chart</a></li>
                        <li class="nav_cat_692 hidden"><a cid="694" href="https://123doc.org/doc-cat/694-duong-line-chart.htm" onmousemove="showNavCat(this, 3)">Đường - Line chart</a></li>
                        <li class="nav_cat_692 hidden"><a cid="695" href="https://123doc.org/doc-cat/695-hinh-tron-pie-chart.htm" onmousemove="showNavCat(this, 3)">Hình Tròn - Pie chart</a></li>
                        <li class="nav_cat_692 hidden"><a cid="696" href="https://123doc.org/doc-cat/696-ma-tran-matrix-chart.htm" onmousemove="showNavCat(this, 3)">Ma Trận - Matrix chart</a></li>
                        <li class="nav_cat_692 hidden"><a cid="697" href="https://123doc.org/doc-cat/697-to-chuc-org-chart.htm" onmousemove="showNavCat(this, 3)">Tổ Chức - Org chart</a></li>
                        <li class="nav_cat_698 hidden"><a cid="699" href="https://123doc.org/doc-cat/699-hinh-khoi-cube.htm" onmousemove="showNavCat(this, 3)">Hình Khối - Cube</a></li>
                        <li class="nav_cat_698 hidden"><a cid="700" href="https://123doc.org/doc-cat/700-kim-tu-thap-pyramid.htm" onmousemove="showNavCat(this, 3)">Kim Tự Tháp - Pyramid</a></li>
                        <li class="nav_cat_698 hidden"><a cid="701" href="https://123doc.org/doc-cat/701-mui-ten-arrow.htm" onmousemove="showNavCat(this, 3)">Mũi Tên - Arrow</a></li>
                        <li class="nav_cat_698 hidden"><a cid="702" href="https://123doc.org/doc-cat/702-hinh-cau-sphere.htm" onmousemove="showNavCat(this, 3)">Hình Cầu - Sphere</a></li>
                        <li class="nav_cat_698 hidden"><a cid="703" href="https://123doc.org/doc-cat/703-banh-xe-gear-wheel.htm" onmousemove="showNavCat(this, 3)">Bánh Xe - Gear Wheel</a></li>
                        <li class="nav_cat_704 hidden"><a cid="705" href="https://123doc.org/doc-cat/705-box-hinh-picture.htm" onmousemove="showNavCat(this, 3)">Box Hình - Picture</a></li>
                        <li class="nav_cat_704 hidden"><a cid="706" href="https://123doc.org/doc-cat/706-box-mui-ten-arrow.htm" onmousemove="showNavCat(this, 3)">Box mũi tên - Arrow</a></li>
                        <li class="nav_cat_704 hidden"><a cid="707" href="https://123doc.org/doc-cat/707-box-vong-tron-circular.htm" onmousemove="showNavCat(this, 3)">Box vòng tròn - Circular</a></li>
                        <li class="nav_cat_704 hidden"><a cid="708" href="https://123doc.org/doc-cat/708-box-ghi-chu-note.htm" onmousemove="showNavCat(this, 3)">Box Ghi Chú - Note</a></li>
                        <li class="nav_cat_704 hidden"><a cid="709" href="https://123doc.org/doc-cat/709-box-chu-nhat-rectangular.htm" onmousemove="showNavCat(this, 3)">Box chữ nhật - Rectangular</a></li>
                        <li class="nav_cat_704 hidden"><a cid="710" href="https://123doc.org/doc-cat/710-box-the-tabbed.htm" onmousemove="showNavCat(this, 3)">Box Thẻ - Tabbed</a></li>
                        <li class="nav_cat_704 hidden"><a cid="711" href="https://123doc.org/doc-cat/711-box-chu-giai-bubbles.htm" onmousemove="showNavCat(this, 3)">Box Chú Giải - Bubbles</a></li>
                        <li class="nav_cat_712 hidden"><a cid="713" href="https://123doc.org/doc-cat/713-hinh-nguoi-3d-man.htm" onmousemove="showNavCat(this, 3)">Hình Người - 3D Man</a></li>
                        <li class="nav_cat_712 hidden"><a cid="714" href="https://123doc.org/doc-cat/714-bieu-tuong-clipart.htm" onmousemove="showNavCat(this, 3)">Biểu Tượng - Clipart</a></li>
                        <li class="nav_cat_712 hidden"><a cid="715" href="https://123doc.org/doc-cat/715-minh-hoa-illustration.htm" onmousemove="showNavCat(this, 3)">Minh Họa - Illustration</a></li>
                        <li class="nav_cat_712 hidden"><a cid="716" href="https://123doc.org/doc-cat/716-hinh-dong-animation.htm" onmousemove="showNavCat(this, 3)">Hình Động - Animation</a></li>
                        <li class="nav_cat_718 hidden"><a cid="719" href="https://123doc.org/doc-cat/719-lich-calendars.htm" onmousemove="showNavCat(this, 3)">Lịch - Calendars</a></li>
                        <li class="nav_cat_718 hidden"><a cid="720" href="https://123doc.org/doc-cat/720-so-do-gantt.htm" onmousemove="showNavCat(this, 3)">Sơ Đồ Gantt</a></li>
                        <li class="nav_cat_718 hidden"><a cid="721" href="https://123doc.org/doc-cat/721-thoi-gian-timlines.htm" onmousemove="showNavCat(this, 3)">Thời Gian - Timlines</a></li>
                        <li class="nav_cat_718 hidden"><a cid="722" href="https://123doc.org/doc-cat/722-cong-viec-phai-lam-to-do.htm" onmousemove="showNavCat(this, 3)">Công Việc Phải Làm - To-do</a></li>
                        <li class="nav_cat_723 hidden"><a cid="724" href="https://123doc.org/doc-cat/724-giao-duc-education.htm" onmousemove="showNavCat(this, 3)">Giáo Dục - Education</a></li>
                        <li class="nav_cat_723 hidden"><a cid="725" href="https://123doc.org/doc-cat/725-kinh-te-economic.htm" onmousemove="showNavCat(this, 3)">Kinh Tế - Economic</a></li>
                        <li class="nav_cat_723 hidden"><a cid="726" href="https://123doc.org/doc-cat/726-khoa-hoc-science.htm" onmousemove="showNavCat(this, 3)">Khoa Học - Science</a></li>
                        <li class="nav_cat_723 hidden"><a cid="727" href="https://123doc.org/doc-cat/727-cong-nghe-technology.htm" onmousemove="showNavCat(this, 3)">Công Nghệ - Technology</a></li>
                        <li class="nav_cat_723 hidden"><a cid="728" href="https://123doc.org/doc-cat/728-van-hoa-culture.htm" onmousemove="showNavCat(this, 3)">Văn Hóa - Culture</a></li>
                        <li class="nav_cat_723 hidden"><a cid="729" href="https://123doc.org/doc-cat/729-thien-nhien-natural.htm" onmousemove="showNavCat(this, 3)">Thiên Nhiên - Natural</a></li>
                        <li class="nav_cat_723 hidden"><a cid="730" href="https://123doc.org/doc-cat/730-dat-nuoc-country.htm" onmousemove="showNavCat(this, 3)">Đất Nước - Country</a></li>
                        <li class="nav_cat_723 hidden"><a cid="731" href="https://123doc.org/doc-cat/731-con-nguoi-people.htm" onmousemove="showNavCat(this, 3)">Con Người - People</a></li>
                        <li class="nav_cat_723 hidden"><a cid="732" href="https://123doc.org/doc-cat/732-nghe-thuat-art-picture.htm" onmousemove="showNavCat(this, 3)">Nghệ Thuật - Art Picture</a></li>
                        <li class="nav_cat_723 hidden"><a cid="733" href="https://123doc.org/doc-cat/733-anh-vui-funny.htm" onmousemove="showNavCat(this, 3)">Ảnh Vui - Funny</a></li>
                        <li class="nav_cat_734 hidden"><a cid="735" href="https://123doc.org/doc-cat/735-van-ban-mau.htm" onmousemove="showNavCat(this, 3)">Văn Bản Mẫu</a></li>
                        <li class="nav_cat_734 hidden"><a cid="736" href="https://123doc.org/doc-cat/736-van-bieu-cam.htm" onmousemove="showNavCat(this, 3)">Văn Biểu Cảm</a></li>
                        <li class="nav_cat_734 hidden"><a cid="737" href="https://123doc.org/doc-cat/737-van-chung-minh.htm" onmousemove="showNavCat(this, 3)">Văn Chứng Minh</a></li>
                        <li class="nav_cat_734 hidden"><a cid="738" href="https://123doc.org/doc-cat/738-van-ke-chuyen.htm" onmousemove="showNavCat(this, 3)">Văn Kể Chuyện</a></li>
                        <li class="nav_cat_734 hidden"><a cid="739" href="https://123doc.org/doc-cat/739-van-mieu-ta.htm" onmousemove="showNavCat(this, 3)">Văn Miêu Tả</a></li>
                        <li class="nav_cat_734 hidden"><a cid="740" href="https://123doc.org/doc-cat/740-van-nghi-luan.htm" onmousemove="showNavCat(this, 3)">Văn Nghị Luận</a></li>
                        <li class="nav_cat_734 hidden"><a cid="741" href="https://123doc.org/doc-cat/741-van-thuyet-minh.htm" onmousemove="showNavCat(this, 3)">Văn Thuyết Minh</a></li>
                        <li class="nav_cat_734 hidden"><a cid="742" href="https://123doc.org/doc-cat/742-van-tu-su.htm" onmousemove="showNavCat(this, 3)">Văn Tự Sự</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1811" href="https://123doc.org/doc-cat/1811-tuyen-sinh-lop-10.htm" onmousemove="showNavCat(this, 3)">Tuyển sinh lớp 10</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1815" href="https://123doc.org/doc-cat/1815-thpt-quoc-gia.htm" onmousemove="showNavCat(this, 3)">THPT Quốc Gia </a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1836" href="https://123doc.org/doc-cat/1836-de-thi-danh-cho-sinh-vien.htm" onmousemove="showNavCat(this, 3)">Đề thi dành cho sinh viên</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1837" href="https://123doc.org/doc-cat/1837-de-thi-tuyen-dung.htm" onmousemove="showNavCat(this, 3)">Đề thi tuyển dụng</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1824" href="https://123doc.org/doc-cat/1824-de-thi-lop-1.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 1</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1825" href="https://123doc.org/doc-cat/1825-de-thi-lop-2.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 2</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1826" href="https://123doc.org/doc-cat/1826-de-thi-lop-3.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 3</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1827" href="https://123doc.org/doc-cat/1827-de-thi-lop-4.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 4</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1840" href="https://123doc.org/doc-cat/1840-de-thi-lop-5.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 5</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1829" href="https://123doc.org/doc-cat/1829-de-thi-lop-6.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 6</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1830" href="https://123doc.org/doc-cat/1830-de-thi-lop-7.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 7</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1831" href="https://123doc.org/doc-cat/1831-de-thi-lop-8.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 8</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1832" href="https://123doc.org/doc-cat/1832-de-thi-lop-9.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 9</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1833" href="https://123doc.org/doc-cat/1833-de-thi-lop-10.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 10</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1834" href="https://123doc.org/doc-cat/1834-de-thi-lop-11.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 11</a></li>
                        <li class="nav_cat_1810 hidden"><a cid="1835" href="https://123doc.org/doc-cat/1835-de-thi-lop-12.htm" onmousemove="showNavCat(this, 3)">Đề thi lớp 12</a></li>
                      </ul>
                      <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 280px;"></div>
                      <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                    </div>
                  </div>
                  <div class="sub_nav_3" data-rel="3">
                    <div class="slimScrollDiv" style="position: relative; overflow: hidden; width: 200px; height: 280px;">
                      <ul style="overflow: hidden; width: 200px; height: 280px;">
                        <li class="nav_cat_307 hidden"><a cid="355" href="https://123doc.org/doc-cat/355-lua-tuoi-3-12-thang.htm" onmousemove="showNavCat(this, 0)">Lứa tuổi 3 - 12 tháng</a></li>
                        <li class="nav_cat_307 hidden"><a cid="356" href="https://123doc.org/doc-cat/356-lua-tuoi-12-24-thang.htm" onmousemove="showNavCat(this, 0)">Lứa tuổi 12 - 24 tháng</a></li>
                        <li class="nav_cat_307 hidden"><a cid="357" href="https://123doc.org/doc-cat/357-lua-tuoi-24-36-thang.htm" onmousemove="showNavCat(this, 0)">Lứa tuổi 24 - 36 tháng</a></li>
                        <li class="nav_cat_307 hidden"><a cid="358" href="https://123doc.org/doc-cat/358-mau-giao-be.htm" onmousemove="showNavCat(this, 0)">Mẫu giáo bé</a></li>
                        <li class="nav_cat_307 hidden"><a cid="359" href="https://123doc.org/doc-cat/359-mau-giao-nho.htm" onmousemove="showNavCat(this, 0)">Mẫu giáo nhỡ</a></li>
                        <li class="nav_cat_307 hidden"><a cid="360" href="https://123doc.org/doc-cat/360-mau-giao-lon.htm" onmousemove="showNavCat(this, 0)">Mẫu giáo lớn</a></li>
                        <li class="nav_cat_366 hidden"><a cid="367" href="https://123doc.org/doc-cat/367-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_366 hidden"><a cid="375" href="https://123doc.org/doc-cat/375-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_366 hidden"><a cid="376" href="https://123doc.org/doc-cat/376-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_366 hidden"><a cid="377" href="https://123doc.org/doc-cat/377-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học</a></li>
                        <li class="nav_cat_366 hidden"><a cid="378" href="https://123doc.org/doc-cat/378-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_366 hidden"><a cid="379" href="https://123doc.org/doc-cat/379-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_366 hidden"><a cid="380" href="https://123doc.org/doc-cat/380-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_366 hidden"><a cid="381" href="https://123doc.org/doc-cat/381-am-nhac.htm" onmousemove="showNavCat(this, 0)">Âm nhạc</a></li>
                        <li class="nav_cat_366 hidden"><a cid="382" href="https://123doc.org/doc-cat/382-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_366 hidden"><a cid="383" href="https://123doc.org/doc-cat/383-my-thuat.htm" onmousemove="showNavCat(this, 0)">Mỹ thuật</a></li>
                        <li class="nav_cat_366 hidden"><a cid="384" href="https://123doc.org/doc-cat/384-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_366 hidden"><a cid="385" href="https://123doc.org/doc-cat/385-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_366 hidden"><a cid="386" href="https://123doc.org/doc-cat/386-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_366 hidden"><a cid="387" href="https://123doc.org/doc-cat/387-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_369 hidden"><a cid="388" href="https://123doc.org/doc-cat/388-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_369 hidden"><a cid="389" href="https://123doc.org/doc-cat/389-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_369 hidden"><a cid="390" href="https://123doc.org/doc-cat/390-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_369 hidden"><a cid="391" href="https://123doc.org/doc-cat/391-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học </a></li>
                        <li class="nav_cat_369 hidden"><a cid="392" href="https://123doc.org/doc-cat/392-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_369 hidden"><a cid="393" href="https://123doc.org/doc-cat/393-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_369 hidden"><a cid="395" href="https://123doc.org/doc-cat/395-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_369 hidden"><a cid="394" href="https://123doc.org/doc-cat/394-am-nhac.htm" onmousemove="showNavCat(this, 0)">Âm nhạc</a></li>
                        <li class="nav_cat_369 hidden"><a cid="396" href="https://123doc.org/doc-cat/396-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_369 hidden"><a cid="397" href="https://123doc.org/doc-cat/397-my-thuat.htm" onmousemove="showNavCat(this, 0)">Mỹ thuật</a></li>
                        <li class="nav_cat_369 hidden"><a cid="398" href="https://123doc.org/doc-cat/398-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_369 hidden"><a cid="399" href="https://123doc.org/doc-cat/399-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_369 hidden"><a cid="400" href="https://123doc.org/doc-cat/400-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_369 hidden"><a cid="401" href="https://123doc.org/doc-cat/401-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_370 hidden"><a cid="402" href="https://123doc.org/doc-cat/402-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_370 hidden"><a cid="403" href="https://123doc.org/doc-cat/403-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_370 hidden"><a cid="404" href="https://123doc.org/doc-cat/404-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_370 hidden"><a cid="405" href="https://123doc.org/doc-cat/405-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học </a></li>
                        <li class="nav_cat_370 hidden"><a cid="406" href="https://123doc.org/doc-cat/406-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_370 hidden"><a cid="407" href="https://123doc.org/doc-cat/407-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_370 hidden"><a cid="408" href="https://123doc.org/doc-cat/408-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_370 hidden"><a cid="409" href="https://123doc.org/doc-cat/409-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_370 hidden"><a cid="410" href="https://123doc.org/doc-cat/410-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_370 hidden"><a cid="411" href="https://123doc.org/doc-cat/411-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_370 hidden"><a cid="412" href="https://123doc.org/doc-cat/412-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_370 hidden"><a cid="413" href="https://123doc.org/doc-cat/413-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_371 hidden"><a cid="414" href="https://123doc.org/doc-cat/414-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_371 hidden"><a cid="415" href="https://123doc.org/doc-cat/415-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_371 hidden"><a cid="416" href="https://123doc.org/doc-cat/416-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_371 hidden"><a cid="417" href="https://123doc.org/doc-cat/417-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học</a></li>
                        <li class="nav_cat_371 hidden"><a cid="418" href="https://123doc.org/doc-cat/418-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_371 hidden"><a cid="419" href="https://123doc.org/doc-cat/419-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_371 hidden"><a cid="420" href="https://123doc.org/doc-cat/420-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_371 hidden"><a cid="421" href="https://123doc.org/doc-cat/421-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_371 hidden"><a cid="422" href="https://123doc.org/doc-cat/422-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_371 hidden"><a cid="423" href="https://123doc.org/doc-cat/423-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_371 hidden"><a cid="424" href="https://123doc.org/doc-cat/424-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_371 hidden"><a cid="425" href="https://123doc.org/doc-cat/425-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_372 hidden"><a cid="426" href="https://123doc.org/doc-cat/426-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_372 hidden"><a cid="368" href="https://123doc.org/doc-cat/368-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_372 hidden"><a cid="427" href="https://123doc.org/doc-cat/427-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_372 hidden"><a cid="428" href="https://123doc.org/doc-cat/428-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học</a></li>
                        <li class="nav_cat_372 hidden"><a cid="429" href="https://123doc.org/doc-cat/429-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_372 hidden"><a cid="430" href="https://123doc.org/doc-cat/430-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_372 hidden"><a cid="431" href="https://123doc.org/doc-cat/431-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_372 hidden"><a cid="432" href="https://123doc.org/doc-cat/432-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_372 hidden"><a cid="433" href="https://123doc.org/doc-cat/433-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_372 hidden"><a cid="435" href="https://123doc.org/doc-cat/435-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_372 hidden"><a cid="434" href="https://123doc.org/doc-cat/434-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_372 hidden"><a cid="436" href="https://123doc.org/doc-cat/436-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_373 hidden"><a cid="437" href="https://123doc.org/doc-cat/437-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_373 hidden"><a cid="438" href="https://123doc.org/doc-cat/438-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_373 hidden"><a cid="439" href="https://123doc.org/doc-cat/439-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_373 hidden"><a cid="440" href="https://123doc.org/doc-cat/440-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học</a></li>
                        <li class="nav_cat_373 hidden"><a cid="441" href="https://123doc.org/doc-cat/441-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_373 hidden"><a cid="442" href="https://123doc.org/doc-cat/442-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_373 hidden"><a cid="443" href="https://123doc.org/doc-cat/443-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_373 hidden"><a cid="444" href="https://123doc.org/doc-cat/444-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_373 hidden"><a cid="445" href="https://123doc.org/doc-cat/445-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_373 hidden"><a cid="446" href="https://123doc.org/doc-cat/446-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_373 hidden"><a cid="447" href="https://123doc.org/doc-cat/447-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_373 hidden"><a cid="448" href="https://123doc.org/doc-cat/448-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_374 hidden"><a cid="449" href="https://123doc.org/doc-cat/449-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ văn</a></li>
                        <li class="nav_cat_374 hidden"><a cid="450" href="https://123doc.org/doc-cat/450-toan-hoc.htm" onmousemove="showNavCat(this, 0)">Toán học</a></li>
                        <li class="nav_cat_374 hidden"><a cid="451" href="https://123doc.org/doc-cat/451-vat-ly.htm" onmousemove="showNavCat(this, 0)">Vật lý</a></li>
                        <li class="nav_cat_374 hidden"><a cid="452" href="https://123doc.org/doc-cat/452-hoa-hoc.htm" onmousemove="showNavCat(this, 0)">Hóa học</a></li>
                        <li class="nav_cat_374 hidden"><a cid="453" href="https://123doc.org/doc-cat/453-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                        <li class="nav_cat_374 hidden"><a cid="454" href="https://123doc.org/doc-cat/454-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_374 hidden"><a cid="456" href="https://123doc.org/doc-cat/456-tieng-anh.htm" onmousemove="showNavCat(this, 0)">Tiếng Anh</a></li>
                        <li class="nav_cat_374 hidden"><a cid="455" href="https://123doc.org/doc-cat/455-tin-hoc.htm" onmousemove="showNavCat(this, 0)">Tin học</a></li>
                        <li class="nav_cat_374 hidden"><a cid="457" href="https://123doc.org/doc-cat/457-cong-nghe.htm" onmousemove="showNavCat(this, 0)">Công nghệ</a></li>
                        <li class="nav_cat_374 hidden"><a cid="458" href="https://123doc.org/doc-cat/458-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_374 hidden"><a cid="459" href="https://123doc.org/doc-cat/459-giao-duc-cong-dan.htm" onmousemove="showNavCat(this, 0)">Giáo dục công dân</a></li>
                        <li class="nav_cat_374 hidden"><a cid="460" href="https://123doc.org/doc-cat/460-the-duc.htm" onmousemove="showNavCat(this, 0)">Thể dục</a></li>
                        <li class="nav_cat_469 hidden"><a cid="478" href="https://123doc.org/doc-cat/478-phap-luat-dai-cuong.htm" onmousemove="showNavCat(this, 0)">Pháp luật đại cương</a></li>
                        <li class="nav_cat_469 hidden"><a cid="479" href="https://123doc.org/doc-cat/479-triet-hoc.htm" onmousemove="showNavCat(this, 0)">Triết học</a></li>
                        <li class="nav_cat_469 hidden"><a cid="480" href="https://123doc.org/doc-cat/480-toan-cao-cap.htm" onmousemove="showNavCat(this, 0)">Toán cao cấp</a></li>
                        <li class="nav_cat_469 hidden"><a cid="481" href="https://123doc.org/doc-cat/481-ly-thuyet-xac-suat-thong-ke.htm" onmousemove="showNavCat(this, 0)">Lý thuyết xác suất - thống kê</a></li>
                        <li class="nav_cat_469 hidden"><a cid="482" href="https://123doc.org/doc-cat/482-kinh-te-luong.htm" onmousemove="showNavCat(this, 0)">Kinh tế lượng</a></li>
                        <li class="nav_cat_469 hidden"><a cid="483" href="https://123doc.org/doc-cat/483-toan-roi-rac.htm" onmousemove="showNavCat(this, 0)">Toán rời rạc</a></li>
                        <li class="nav_cat_469 hidden"><a cid="484" href="https://123doc.org/doc-cat/484-chu-nghi-xa-hoi.htm" onmousemove="showNavCat(this, 0)">Chủ nghĩ xã hội </a></li>
                        <li class="nav_cat_469 hidden"><a cid="485" href="https://123doc.org/doc-cat/485-kinh-te-chinh-tri.htm" onmousemove="showNavCat(this, 0)">Kinh tế chính trị</a></li>
                        <li class="nav_cat_469 hidden"><a cid="486" href="https://123doc.org/doc-cat/486-tu-tuong-ho-chi-minh.htm" onmousemove="showNavCat(this, 0)">Tư tưởng Hồ Chí Minh</a></li>
                        <li class="nav_cat_469 hidden"><a cid="487" href="https://123doc.org/doc-cat/487-duong-loi-cach-mang-dang-cong-san-viet-nam.htm" onmousemove="showNavCat(this, 0)">Đường lối cách mạng Đảng cộng sản Việt Nam</a></li>
                        <li class="nav_cat_469 hidden"><a cid="488" href="https://123doc.org/doc-cat/488-kinh-te-vi-mo.htm" onmousemove="showNavCat(this, 0)">Kinh tế vi mô</a></li>
                        <li class="nav_cat_469 hidden"><a cid="489" href="https://123doc.org/doc-cat/489-kinh-te-vi-mo.htm" onmousemove="showNavCat(this, 0)">Kinh tế vĩ mô</a></li>
                        <li class="nav_cat_469 hidden"><a cid="490" href="https://123doc.org/doc-cat/490-logic-hoc.htm" onmousemove="showNavCat(this, 0)">Logic học</a></li>
                        <li class="nav_cat_469 hidden"><a cid="491" href="https://123doc.org/doc-cat/491-phuong-phap-hoc-tap-va-nghien-cuu-khoa-hoc.htm" onmousemove="showNavCat(this, 0)">Phương pháp học tập và nghiên cứu khoa học</a></li>
                        <li class="nav_cat_469 hidden"><a cid="492" href="https://123doc.org/doc-cat/492-tin-hoc-dai-cuong.htm" onmousemove="showNavCat(this, 0)">Tin học đại cương</a></li>
                        <li class="nav_cat_469 hidden"><a cid="493" href="https://123doc.org/doc-cat/493-marketing-can-ban.htm" onmousemove="showNavCat(this, 0)">Marketing căn bản</a></li>
                        <li class="nav_cat_469 hidden"><a cid="494" href="https://123doc.org/doc-cat/494-ly-thuyet-tai-chinh-tien-te.htm" onmousemove="showNavCat(this, 0)">Lý thuyết tài chính tiền tệ</a></li>
                        <li class="nav_cat_470 hidden"><a cid="495" href="https://123doc.org/doc-cat/495-noi-khoa.htm" onmousemove="showNavCat(this, 0)">Nội khoa</a></li>
                        <li class="nav_cat_470 hidden"><a cid="496" href="https://123doc.org/doc-cat/496-ngoai-khoa.htm" onmousemove="showNavCat(this, 0)">Ngoại khoa</a></li>
                        <li class="nav_cat_470 hidden"><a cid="497" href="https://123doc.org/doc-cat/497-san-phu-khoa.htm" onmousemove="showNavCat(this, 0)">Sản phụ khoa</a></li>
                        <li class="nav_cat_470 hidden"><a cid="498" href="https://123doc.org/doc-cat/498-nhi-khoa.htm" onmousemove="showNavCat(this, 0)">Nhi khoa</a></li>
                        <li class="nav_cat_470 hidden"><a cid="499" href="https://123doc.org/doc-cat/499-dieu-duong.htm" onmousemove="showNavCat(this, 0)">Điều dưỡng</a></li>
                        <li class="nav_cat_470 hidden"><a cid="500" href="https://123doc.org/doc-cat/500-nhan-khoa.htm" onmousemove="showNavCat(this, 0)">Nhãn khoa</a></li>
                        <li class="nav_cat_470 hidden"><a cid="501" href="https://123doc.org/doc-cat/501-rang-ham-mat.htm" onmousemove="showNavCat(this, 0)">Răng - Hàm - Mặt</a></li>
                        <li class="nav_cat_470 hidden"><a cid="502" href="https://123doc.org/doc-cat/502-chan-doan-hinh-anh.htm" onmousemove="showNavCat(this, 0)">Chẩn đoán hình ảnh</a></li>
                        <li class="nav_cat_470 hidden"><a cid="503" href="https://123doc.org/doc-cat/503-tai-mui-hong.htm" onmousemove="showNavCat(this, 0)">Tai - Mũi - Họng</a></li>
                        <li class="nav_cat_470 hidden"><a cid="504" href="https://123doc.org/doc-cat/504-gay-me-hoi-suc.htm" onmousemove="showNavCat(this, 0)">Gây mê hồi sức</a></li>
                        <li class="nav_cat_470 hidden"><a cid="505" href="https://123doc.org/doc-cat/505-da-lieu.htm" onmousemove="showNavCat(this, 0)">Da liễu</a></li>
                        <li class="nav_cat_470 hidden"><a cid="506" href="https://123doc.org/doc-cat/506-vi-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Vi sinh học</a></li>
                        <li class="nav_cat_470 hidden"><a cid="507" href="https://123doc.org/doc-cat/507-truyen-nhiem.htm" onmousemove="showNavCat(this, 0)">Truyền nhiễm</a></li>
                        <li class="nav_cat_470 hidden"><a cid="508" href="https://123doc.org/doc-cat/508-huyet-hoc-truyen-mau.htm" onmousemove="showNavCat(this, 0)">Huyết học - Truyền máu</a></li>
                        <li class="nav_cat_470 hidden"><a cid="509" href="https://123doc.org/doc-cat/509-tam-than.htm" onmousemove="showNavCat(this, 0)">Tâm thần</a></li>
                        <li class="nav_cat_470 hidden"><a cid="510" href="https://123doc.org/doc-cat/510-y-hoc-co-truyen.htm" onmousemove="showNavCat(this, 0)">Y học cổ truyền</a></li>
                        <li class="nav_cat_470 hidden"><a cid="511" href="https://123doc.org/doc-cat/511-y-hoc-gia-dinh.htm" onmousemove="showNavCat(this, 0)">Y học gia đình</a></li>
                        <li class="nav_cat_470 hidden"><a cid="512" href="https://123doc.org/doc-cat/512-y-hoc-cong-cong.htm" onmousemove="showNavCat(this, 0)">Y học công cộng</a></li>
                        <li class="nav_cat_470 hidden"><a cid="513" href="https://123doc.org/doc-cat/513-bao-che.htm" onmousemove="showNavCat(this, 0)">Bào chế</a></li>
                        <li class="nav_cat_470 hidden"><a cid="514" href="https://123doc.org/doc-cat/514-hoa-duoc.htm" onmousemove="showNavCat(this, 0)">Hóa dược</a></li>
                        <li class="nav_cat_471 hidden"><a cid="515" href="https://123doc.org/doc-cat/515-thi-truong-chung-khoan.htm" onmousemove="showNavCat(this, 0)">Thị trường chứng khoán</a></li>
                        <li class="nav_cat_471 hidden"><a cid="516" href="https://123doc.org/doc-cat/516-nguyen-ly-ke-toan.htm" onmousemove="showNavCat(this, 0)">Nguyên lý kế toán</a></li>
                        <li class="nav_cat_471 hidden"><a cid="517" href="https://123doc.org/doc-cat/517-ke-toan-tai-chinh.htm" onmousemove="showNavCat(this, 0)">Kế toán tài chính</a></li>
                        <li class="nav_cat_471 hidden"><a cid="518" href="https://123doc.org/doc-cat/518-ke-toan-ngan-hang-thuong-mai.htm" onmousemove="showNavCat(this, 0)">Kế toán ngân hàng thương mại</a></li>
                        <li class="nav_cat_471 hidden"><a cid="519" href="https://123doc.org/doc-cat/519-ke-toan-quan-tri.htm" onmousemove="showNavCat(this, 0)">Kế toán quản trị</a></li>
                        <li class="nav_cat_471 hidden"><a cid="520" href="https://123doc.org/doc-cat/520-thanh-toan-quoc-te.htm" onmousemove="showNavCat(this, 0)">Thanh toán quốc tế</a></li>
                        <li class="nav_cat_471 hidden"><a cid="521" href="https://123doc.org/doc-cat/521-thue.htm" onmousemove="showNavCat(this, 0)">Thuế</a></li>
                        <li class="nav_cat_471 hidden"><a cid="522" href="https://123doc.org/doc-cat/522-ly-thuyet-kiem-toan.htm" onmousemove="showNavCat(this, 0)">Lý thuyết kiểm toán</a></li>
                        <li class="nav_cat_471 hidden"><a cid="523" href="https://123doc.org/doc-cat/523-kiem-toan-hanh-chinh-su-nghiep.htm" onmousemove="showNavCat(this, 0)">Kiểm toán hành chính sự nghiệp</a></li>
                        <li class="nav_cat_471 hidden"><a cid="524" href="https://123doc.org/doc-cat/524-quan-tri-tai-chinh-doanh-nghiep.htm" onmousemove="showNavCat(this, 0)">Quản trị tài chính doanh nghiệp</a></li>
                        <li class="nav_cat_471 hidden"><a cid="525" href="https://123doc.org/doc-cat/525-kiem-toan-phan-hanh.htm" onmousemove="showNavCat(this, 0)">Kiểm toán phần hành</a></li>
                        <li class="nav_cat_471 hidden"><a cid="526" href="https://123doc.org/doc-cat/526-tai-chinh-cong.htm" onmousemove="showNavCat(this, 0)">Tài chính công</a></li>
                        <li class="nav_cat_471 hidden"><a cid="527" href="https://123doc.org/doc-cat/527-dau-tu-quoc-te.htm" onmousemove="showNavCat(this, 0)">Đầu tư quốc tế</a></li>
                        <li class="nav_cat_471 hidden"><a cid="528" href="https://123doc.org/doc-cat/528-tham-dinh-du-an-dau-tu.htm" onmousemove="showNavCat(this, 0)">Thẩm định dự án đầu tư</a></li>
                        <li class="nav_cat_471 hidden"><a cid="529" href="https://123doc.org/doc-cat/529-kinh-te-cong-cong.htm" onmousemove="showNavCat(this, 0)">Kinh tế công cộng</a></li>
                        <li class="nav_cat_471 hidden"><a cid="530" href="https://123doc.org/doc-cat/530-kinh-te-moi-truong.htm" onmousemove="showNavCat(this, 0)">Kinh tế môi trường</a></li>
                        <li class="nav_cat_471 hidden"><a cid="531" href="https://123doc.org/doc-cat/531-thi-truong-tai-chinh.htm" onmousemove="showNavCat(this, 0)">Thị trường tài chính</a></li>
                        <li class="nav_cat_471 hidden"><a cid="532" href="https://123doc.org/doc-cat/532-phan-tich-tai-chinh-doanh-nghiep.htm" onmousemove="showNavCat(this, 0)">Phân tích tài chính doanh nghiệp</a></li>
                        <li class="nav_cat_471 hidden"><a cid="533" href="https://123doc.org/doc-cat/533-van-tai-trong-ngoai-thuong.htm" onmousemove="showNavCat(this, 0)">Vận tải trong ngoại thương</a></li>
                        <li class="nav_cat_471 hidden"><a cid="534" href="https://123doc.org/doc-cat/534-giao-dich-thuong-mai-quoc-te.htm" onmousemove="showNavCat(this, 0)">Giao dịch thương mại quốc tế</a></li>
                        <li class="nav_cat_471 hidden"><a cid="535" href="https://123doc.org/doc-cat/535-marketing-quoc-te.htm" onmousemove="showNavCat(this, 0)">Marketing quốc tế</a></li>
                        <li class="nav_cat_471 hidden"><a cid="536" href="https://123doc.org/doc-cat/536-bao-hiem.htm" onmousemove="showNavCat(this, 0)">Bảo hiểm</a></li>
                        <li class="nav_cat_471 hidden"><a cid="537" href="https://123doc.org/doc-cat/537-hai-quan.htm" onmousemove="showNavCat(this, 0)">Hải quan</a></li>
                        <li class="nav_cat_471 hidden"><a cid="538" href="https://123doc.org/doc-cat/538-dich-vu-du-lich.htm" onmousemove="showNavCat(this, 0)">Dịch vụ - Du lịch</a></li>
                        <li class="nav_cat_472 hidden"><a cid="539" href="https://123doc.org/doc-cat/539-quan-he-quoc-te.htm" onmousemove="showNavCat(this, 0)">Quan hệ quốc tế</a></li>
                        <li class="nav_cat_472 hidden"><a cid="540" href="https://123doc.org/doc-cat/540-giao-duc-hoc.htm" onmousemove="showNavCat(this, 0)">Giáo dục học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="541" href="https://123doc.org/doc-cat/541-dong-phuong-hoc.htm" onmousemove="showNavCat(this, 0)">Đông phương học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="542" href="https://123doc.org/doc-cat/542-van-hoa-lich-su.htm" onmousemove="showNavCat(this, 0)">Văn hóa - Lịch sử</a></li>
                        <li class="nav_cat_472 hidden"><a cid="543" href="https://123doc.org/doc-cat/543-xa-hoi-hoc.htm" onmousemove="showNavCat(this, 0)">Xã hội học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="544" href="https://123doc.org/doc-cat/544-bao-chi.htm" onmousemove="showNavCat(this, 0)">Báo chí</a></li>
                        <li class="nav_cat_472 hidden"><a cid="545" href="https://123doc.org/doc-cat/545-dia-ly-hoc.htm" onmousemove="showNavCat(this, 0)">Địa lý học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="546" href="https://123doc.org/doc-cat/546-quan-ly-do-thi-dat-dai-cong-tac-xa-hoi.htm" onmousemove="showNavCat(this, 0)">Quản lý đô thị - Đất đai - Công tác xã hội</a></li>
                        <li class="nav_cat_472 hidden"><a cid="547" href="https://123doc.org/doc-cat/547-nhan-hoc-tam-ly-hoc.htm" onmousemove="showNavCat(this, 0)">Nhân học - Tâm lý học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="548" href="https://123doc.org/doc-cat/548-viet-nam-hoc.htm" onmousemove="showNavCat(this, 0)">Việt Nam học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="549" href="https://123doc.org/doc-cat/549-van-hoc-ngon-ngu-hoc.htm" onmousemove="showNavCat(this, 0)">Văn học - Ngôn ngữ học</a></li>
                        <li class="nav_cat_472 hidden"><a cid="591" href="https://123doc.org/doc-cat/591-hanh-chinh-van-thu.htm" onmousemove="showNavCat(this, 0)">Hành chính - Văn thư</a></li>
                        <li class="nav_cat_473 hidden"><a cid="550" href="https://123doc.org/doc-cat/550-luat-hinh-su-luat-to-tung-hinh-su.htm" onmousemove="showNavCat(this, 0)">Luật hình sự - Luật tố tụng hình sự</a></li>
                        <li class="nav_cat_473 hidden"><a cid="551" href="https://123doc.org/doc-cat/551-luat-thuong-mai.htm" onmousemove="showNavCat(this, 0)">Luật thương mại</a></li>
                        <li class="nav_cat_473 hidden"><a cid="552" href="https://123doc.org/doc-cat/552-luat-doanh-nghiep.htm" onmousemove="showNavCat(this, 0)">Luật doanh nghiệp</a></li>
                        <li class="nav_cat_473 hidden"><a cid="553" href="https://123doc.org/doc-cat/553-luat-tai-nguyen-moi-truong.htm" onmousemove="showNavCat(this, 0)">Luật tài nguyên môi trường</a></li>
                        <li class="nav_cat_473 hidden"><a cid="554" href="https://123doc.org/doc-cat/554-luat-dan-su.htm" onmousemove="showNavCat(this, 0)">Luật dân sự</a></li>
                        <li class="nav_cat_474 hidden"><a cid="555" href="https://123doc.org/doc-cat/555-lap-trinh-ung-dung.htm" onmousemove="showNavCat(this, 0)">Lập trình ứng dụng</a></li>
                        <li class="nav_cat_474 hidden"><a cid="556" href="https://123doc.org/doc-cat/556-lap-trinh-web.htm" onmousemove="showNavCat(this, 0)">Lập trình web</a></li>
                        <li class="nav_cat_474 hidden"><a cid="557" href="https://123doc.org/doc-cat/557-lap-trinh-ung-dung-di-dong.htm" onmousemove="showNavCat(this, 0)">Lập trình ứng dụng di động</a></li>
                        <li class="nav_cat_474 hidden"><a cid="558" href="https://123doc.org/doc-cat/558-ngon-ngu-nhung-va-mot-so-ngon-ngu-khac.htm" onmousemove="showNavCat(this, 0)">Ngôn ngữ nhúng và một số ngôn ngữ khác</a></li>
                        <li class="nav_cat_474 hidden"><a cid="559" href="https://123doc.org/doc-cat/559-database.htm" onmousemove="showNavCat(this, 0)">Database</a></li>
                        <li class="nav_cat_474 hidden"><a cid="560" href="https://123doc.org/doc-cat/560-lap-trinh-tren-social-network-platform.htm" onmousemove="showNavCat(this, 0)">Lập trình trên social network platform</a></li>
                        <li class="nav_cat_474 hidden"><a cid="561" href="https://123doc.org/doc-cat/561-ma-hoa-giai-ma-va-thuat-toan.htm" onmousemove="showNavCat(this, 0)">Mã hóa - Giải mã và thuật toán</a></li>
                        <li class="nav_cat_474 hidden"><a cid="562" href="https://123doc.org/doc-cat/562-mang-can-ban.htm" onmousemove="showNavCat(this, 0)">Mạng căn bản</a></li>
                        <li class="nav_cat_474 hidden"><a cid="563" href="https://123doc.org/doc-cat/563-chuyen-de-mang-khong-day.htm" onmousemove="showNavCat(this, 0)">Chuyên đề mạng không dây</a></li>
                        <li class="nav_cat_474 hidden"><a cid="564" href="https://123doc.org/doc-cat/564-quan-tri-mang-linux.htm" onmousemove="showNavCat(this, 0)">Quản trị mạng Linux</a></li>
                        <li class="nav_cat_474 hidden"><a cid="565" href="https://123doc.org/doc-cat/565-quan-tri-mang-windows.htm" onmousemove="showNavCat(this, 0)">Quản trị mạng Windows</a></li>
                        <li class="nav_cat_474 hidden"><a cid="566" href="https://123doc.org/doc-cat/566-he-thong-mang-cisco.htm" onmousemove="showNavCat(this, 0)">Hệ thống mạng Cisco</a></li>
                        <li class="nav_cat_474 hidden"><a cid="567" href="https://123doc.org/doc-cat/567-bao-mat.htm" onmousemove="showNavCat(this, 0)">Bảo mật</a></li>
                        <li class="nav_cat_475 hidden"><a cid="568" href="https://123doc.org/doc-cat/568-su-pham-mam-non.htm" onmousemove="showNavCat(this, 0)">Sư phạm mầm non</a></li>
                        <li class="nav_cat_475 hidden"><a cid="569" href="https://123doc.org/doc-cat/569-su-pham-tieu-hoc.htm" onmousemove="showNavCat(this, 0)">Sư phạm tiểu học</a></li>
                        <li class="nav_cat_475 hidden"><a cid="570" href="https://123doc.org/doc-cat/570-su-pham-toan.htm" onmousemove="showNavCat(this, 0)">Sư phạm toán</a></li>
                        <li class="nav_cat_475 hidden"><a cid="571" href="https://123doc.org/doc-cat/571-su-pham-vat-ly.htm" onmousemove="showNavCat(this, 0)">Sư phạm vật lý</a></li>
                        <li class="nav_cat_475 hidden"><a cid="572" href="https://123doc.org/doc-cat/572-su-pham-hoa.htm" onmousemove="showNavCat(this, 0)">Sư phạm hóa</a></li>
                        <li class="nav_cat_475 hidden"><a cid="573" href="https://123doc.org/doc-cat/573-su-pham-van.htm" onmousemove="showNavCat(this, 0)">Sư phạm văn</a></li>
                        <li class="nav_cat_475 hidden"><a cid="574" href="https://123doc.org/doc-cat/574-su-pham-ngoai-ngu.htm" onmousemove="showNavCat(this, 0)">Sư phạm ngoại ngữ</a></li>
                        <li class="nav_cat_475 hidden"><a cid="575" href="https://123doc.org/doc-cat/575-su-pham-su.htm" onmousemove="showNavCat(this, 0)">Sư phạm sử</a></li>
                        <li class="nav_cat_475 hidden"><a cid="576" href="https://123doc.org/doc-cat/576-su-pham-dia.htm" onmousemove="showNavCat(this, 0)">Sư phạm địa</a></li>
                        <li class="nav_cat_475 hidden"><a cid="577" href="https://123doc.org/doc-cat/577-su-pham-sinh.htm" onmousemove="showNavCat(this, 0)">Sư phạm sinh</a></li>
                        <li class="nav_cat_475 hidden"><a cid="578" href="https://123doc.org/doc-cat/578-quan-ly-giao-duc.htm" onmousemove="showNavCat(this, 0)">Quản lý giáo dục</a></li>
                        <li class="nav_cat_476 hidden"><a cid="579" href="https://123doc.org/doc-cat/579-thiet-ke-kien-truc-quy-hoach.htm" onmousemove="showNavCat(this, 0)">Thiết kế kiến trúc - Quy hoạch</a></li>
                        <li class="nav_cat_476 hidden"><a cid="580" href="https://123doc.org/doc-cat/580-thiet-ke-noi-that.htm" onmousemove="showNavCat(this, 0)">Thiết kế nội thất</a></li>
                        <li class="nav_cat_476 hidden"><a cid="581" href="https://123doc.org/doc-cat/581-thiet-ke-ngoai-that.htm" onmousemove="showNavCat(this, 0)">Thiết kế ngoại thất</a></li>
                        <li class="nav_cat_476 hidden"><a cid="582" href="https://123doc.org/doc-cat/582-mau-sac-kien-truc.htm" onmousemove="showNavCat(this, 0)">Màu sắc kiến trúc</a></li>
                        <li class="nav_cat_476 hidden"><a cid="583" href="https://123doc.org/doc-cat/583-ky-thuat-nen-mong-tang-ham.htm" onmousemove="showNavCat(this, 0)">Kỹ thuật nền móng - Tầng hầm</a></li>
                        <li class="nav_cat_476 hidden"><a cid="584" href="https://123doc.org/doc-cat/584-cong-trinh-giao-thong-thuy-loi.htm" onmousemove="showNavCat(this, 0)">Công trình giao thông, thủy lợi</a></li>
                        <li class="nav_cat_476 hidden"><a cid="585" href="https://123doc.org/doc-cat/585-ket-cau-thi-cong-cong-trinh.htm" onmousemove="showNavCat(this, 0)">Kết cấu - Thi công công trình</a></li>
                        <li class="nav_cat_476 hidden"><a cid="586" href="https://123doc.org/doc-cat/586-quy-hoach-va-khao-sat-xay-dung.htm" onmousemove="showNavCat(this, 0)">Quy hoạch và khảo sát xây dựng </a></li>
                        <li class="nav_cat_476 hidden"><a cid="587" href="https://123doc.org/doc-cat/587-thi-cong-nghiem-thu-va-thiet-bi-xay-dung.htm" onmousemove="showNavCat(this, 0)">Thi công - Nghiệm thu và Thiết bị xây dựng</a></li>
                        <li class="nav_cat_476 hidden"><a cid="588" href="https://123doc.org/doc-cat/588-vat-lieu-xay-dung.htm" onmousemove="showNavCat(this, 0)">Vật liệu xây dựng</a></li>
                        <li class="nav_cat_476 hidden"><a cid="589" href="https://123doc.org/doc-cat/589-van-ban-phap-luat-quy-chuan-xay-dung.htm" onmousemove="showNavCat(this, 0)">Văn bản pháp luật, quy chuẩn xây dựng</a></li>
                        <li class="nav_cat_476 hidden"><a cid="590" href="https://123doc.org/doc-cat/590-phong-thuy.htm" onmousemove="showNavCat(this, 0)">Phong thủy</a></li>
                        <li class="nav_cat_477 hidden"><a cid="592" href="https://123doc.org/doc-cat/592-co-khi-luyen-kim.htm" onmousemove="showNavCat(this, 0)">Cơ khí - Luyện kim</a></li>
                        <li class="nav_cat_477 hidden"><a cid="593" href="https://123doc.org/doc-cat/593-dien-dien-tu-vien-thong.htm" onmousemove="showNavCat(this, 0)">Điện - Điện tử - Viễn thông</a></li>
                        <li class="nav_cat_477 hidden"><a cid="594" href="https://123doc.org/doc-cat/594-hoa-dau-tau-thuy.htm" onmousemove="showNavCat(this, 0)">Hóa dầu - Tàu thủy</a></li>
                        <li class="nav_cat_477 hidden"><a cid="595" href="https://123doc.org/doc-cat/595-co-dien-tu.htm" onmousemove="showNavCat(this, 0)">Cơ điện tử</a></li>
                        <li class="nav_cat_477 hidden"><a cid="596" href="https://123doc.org/doc-cat/596-hang-khong.htm" onmousemove="showNavCat(this, 0)">Hàng không</a></li>
                        <li class="nav_cat_477 hidden"><a cid="597" href="https://123doc.org/doc-cat/597-dieu-khien-va-tu-dong-hoa.htm" onmousemove="showNavCat(this, 0)">Điều khiển và tự động hóa</a></li>
                        <li class="nav_cat_477 hidden"><a cid="598" href="https://123doc.org/doc-cat/598-ky-thuat-hat-nhan.htm" onmousemove="showNavCat(this, 0)">Kỹ thuật hạt nhân</a></li>
                        <li class="nav_cat_477 hidden"><a cid="599" href="https://123doc.org/doc-cat/599-ky-thuat-nhiet-lanh.htm" onmousemove="showNavCat(this, 0)">Kỹ thuật nhiệt lạnh</a></li>
                        <li class="nav_cat_477 hidden"><a cid="600" href="https://123doc.org/doc-cat/600-cong-nghe-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Công nghệ sinh học</a></li>
                        <li class="nav_cat_477 hidden"><a cid="601" href="https://123doc.org/doc-cat/601-cong-nghe-thuc-pham.htm" onmousemove="showNavCat(this, 0)">Công nghệ thực phẩm</a></li>
                        <li class="nav_cat_1811 hidden"><a cid="1812" href="https://123doc.org/doc-cat/1812-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ Văn</a></li>
                        <li class="nav_cat_1811 hidden"><a cid="1813" href="https://123doc.org/doc-cat/1813-toan.htm" onmousemove="showNavCat(this, 0)">Toán</a></li>
                        <li class="nav_cat_1811 hidden"><a cid="1814" href="https://123doc.org/doc-cat/1814-ngoai-ngu.htm" onmousemove="showNavCat(this, 0)">Ngoại Ngữ</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1816" href="https://123doc.org/doc-cat/1816-toan.htm" onmousemove="showNavCat(this, 0)">Toán</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1817" href="https://123doc.org/doc-cat/1817-ly.htm" onmousemove="showNavCat(this, 0)">Lý</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1818" href="https://123doc.org/doc-cat/1818-hoa.htm" onmousemove="showNavCat(this, 0)">Hóa</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1819" href="https://123doc.org/doc-cat/1819-ngoai-ngu.htm" onmousemove="showNavCat(this, 0)">Ngoại Ngữ</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1820" href="https://123doc.org/doc-cat/1820-ngu-van.htm" onmousemove="showNavCat(this, 0)">Ngữ Văn</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1821" href="https://123doc.org/doc-cat/1821-dia-ly.htm" onmousemove="showNavCat(this, 0)">Địa lý</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1822" href="https://123doc.org/doc-cat/1822-lich-su.htm" onmousemove="showNavCat(this, 0)">Lịch sử</a></li>
                        <li class="nav_cat_1815 hidden"><a cid="1823" href="https://123doc.org/doc-cat/1823-sinh-hoc.htm" onmousemove="showNavCat(this, 0)">Sinh học</a></li>
                      </ul>
                      <div class="slimScrollBar" style="background: rgb(0, 0, 0); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: none; border-radius: 7px; z-index: 99; right: 1px; height: 280px;"></div>
                      <div class="slimScrollRail" style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: rgb(51, 51, 51); opacity: 0.2; z-index: 90; right: 1px;"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </span>
        </div>
        <div class="headerRight">
         <div class="searchBox">
           <div class="selectOption">
             <a href="#">Tài liệu</a>
           </div>
           <input type="text" id="txtSearch" placeholder="Tìm kiếm ..." name="txtSearch"/>
           <a href="#" id="txticonSearch"><i class="icon icon_search"></i></a>

           <script language="javascript">
            var button = document.getElementById("txticonSearch");
            var cn = "https://123doc.org//global/home/search.php/s?&q=";
            button.onclick = function()
            {
              var input = document.getElementById("txtSearch").value;
              var cnt = cn + input;
              window.location.assign(cnt);
            };
          </script>

         </div>
         <a href="https://123doc.org/gioi-thieu.htm" class="landBox col-lg-1 col-12"><span class="icon i_start"></span></a>
       </div>

     <div class="collapse navbar-collapse" id="navbarResponsive">
      <?php 
      wp_nav_menu( array(
        'theme_location'  => 'header-menu', // Gọi menu đã đăng ký trong function
        'depth'           => 2,     // Cấu hình dropdown 2 cấp
        'container'       => false, // Thẻ div bọc menu
        'menu_class'      => 'navbar-nav ml-0', // Class của nav bootstrap
        'fallback_cb'     => 'WP_Bootstrap_Navwalker::fallback',
        'walker'          => new WP_Bootstrap_Navwalker()
      ));
      ?>
    </div>
  </div>
</div>
</nav>
<?php mini_blog_breadcrumbs() ?>
